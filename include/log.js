/**
 * ������� ������ log, ������� ���-����, ��������� � ���� ������ ������
 *
 * @param [options] {object[]} - ������ �������� � ������������� ��������� ����������
 * @returns {AISAutomation.log}
 */
AISAutomation.log = function(options) {
  var options = options || {};
  if (!options['file']) throw new Error('�� ������� ��� ���-�����');
  if (!options['dir']) throw new Error('�� ������� ��� ���������� ��� �������� ���-������');
  this.by_date = typeof(options.by_date) == 'undefined' ? true : options.by_date;
  this.unique_name = typeof options.unique_name == 'undefined' ? true : options.unique_name;
  if (Array.isArray(options.level)) {
    this.level = options.level;
  } else {
    throw new Error('������ ����������� ���-��������� (level) ������ ���� �������� � ���� �������');
  }
  
  var d = new Date(); this.date_info = {};
  this.date_info = {
    year: this.leadingZero(d.getFullYear(), 4),
    month: this.leadingZero(d.getMonth() + 1, 2),
    day: this.leadingZero(d.getDate(), 2),
    hour: this.leadingZero(d.getHours(), 2),
    minute: this.leadingZero(d.getMinutes(), 2),
    second: this.leadingZero(d.getSeconds(), 2)
  }
  
  this.dir = options['dir'];
  this.file = options['file'];
  if (this.by_date) this.dir += '\\' + this.date_info.year + '\\' + this.date_info.month + '\\' + this.date_info.day;
  if (this.unique_name) this.file = this.date_info.year + '-' + this.date_info.month + '-' + this.date_info.day + '-' + this.date_info.hour + '-' + this.date_info.minute + '-' + this.date_info.second + ' ' + this.file;

  AISAutomation.fs.createFolder(this.dir);
  this.file = AISAutomation.fs.fso.CreateTextFile(this.dir + '\\' + this.file, true, false);
  this.add('������', 'info');
  return this;
};

/**
 * ���������� ����� � ������ ����������� ������� �����
 *
 * @param [num] {integer} - �������������� �����
 * @param [zeroes_count] {integer} - �������� ���������� ������� �����
 * @returns {string}
 */
AISAutomation.log.prototype.leadingZero = function (num, zeroes_count) {
  if (zeroes_count < 0) throw new Error('���������� ������� ����� �� ����� ���� ������ ����');
  var zeroes = '';
  var num_length = String(num).length;
  var slice_count = num_length > zeroes_count ? num_length : zeroes_count;
  for (var i = 0; i < zeroes_count; i++) zeroes += '0';
  return (zeroes + num).slice(slice_count * (-1));
};

/**
 * ��������� � ���-���� � ������� � ������� ���������. ���� ��� ��������� ������ ��� error, �� ���������� ������
 *
 * @param [message] {string} - ����� ���������
 * @param [msg_type] {string} - ���� �� ����� ���������: 'info', 'warn', 'debug', 'error'
 * @returns {AISAutomation.log}
 */
AISAutomation.log.prototype.add = function(message, msg_type) {
  msg_type = msg_type || 'info';
  msg_type = msg_type.toLowerCase();
  for (var i = 0; i < this.level.length; i++) {
    var d = new Date();
    var timestamp = this.leadingZero(d.getFullYear(), 4) + '-' + this.leadingZero(d.getMonth() + 1, 2) + '-' + this.leadingZero(d.getDate(), 2) +
      ' ' + this.leadingZero(d.getHours(), 2) + ':' + this.leadingZero(d.getMinutes(), 2) + ':' +
      this.leadingZero(d.getSeconds(), 2) + '. ';
    var typestamp = msg_type.toUpperCase() + ': ';
    if (msg_type == this.level[i].toLowerCase()) {
      var out_message = timestamp + typestamp + message;
      this.file.WriteLine(out_message);
      WScript.Echo(out_message);
      break;
    }
  }
  if (msg_type === 'error') {
    this.close();
    throw new Error(message);
  }
  return this;
};

AISAutomation.log.prototype.close = function() {
  this.add('����������', 'info');
  this.file.Close();
};