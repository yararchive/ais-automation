/**
 * ������� ������ db, ���������� ���������
 *
 * @param [options] {object[]} - ������ �������� � ������������� ��������� ����������
 * @param [log] {AISAutomation.log} - ���, � ������� ������� ��������� �� ��
 * @returns {AISAutomation.db}
 */
AISAutomation.db = function (options, log) {
  if (!AISAutomation.log.prototype.isPrototypeOf(log)) throw new Error('� ������ AISAutomation.db ������ ���� ������� �������� log ���� AISAutomation.log');
  this.log = log;
  var options = options || {};
  if (!options['server']) throw new Error('�� ������ ������ �� (�������� server)');
  if (!options['name']) throw new Error('�� ������� ��� �� (�������� name)');
  if (!options['user']) throw new Error('�� ������ ������������ �� (�������� user)');
  this.server = options.server;
  this.name = options.name;
  this.user = options.user;
  this.password = options.password || '';
  if (options.max_reconnects === parseInt(options.max_reconnects)) {
    this.max_reconnects = Math.abs(options.max_reconnects);
  } else {
    throw new Error('�������� max_reconnects ������ ���� ����� ������');
  }
  if (options.reconnect_interval === parseInt(options.reconnect_interval)) {
    this.reconnect_interval = Math.abs(options.reconnect_interval);
  } else {
    throw new Error('�������� reconnect_interval ������ ���� ����� ������');
  }
  this.connect_tries = 0; // ���������� ������� ����������� � ������� ��
  this.result = null; // ��������� ���������� �������
};

/**
 * ����������� � ��, ��������� � ����������
 *
 * @returns {AISAutomation.db}
 */
AISAutomation.db.prototype.connect = function() {
  this.connection = new ActiveXObject('ADODB.Connection');
  try {
    this.connect_tries++;
    this.log.add('������� ����������� � ' + this.connect_tries + ' � �� ' + this.name + ' �� ������� ' + this.server, 'info');
    this.connection.open('Provider=SQLOLEDB;Initial Catalog=' + this.name + ';uid=' + this.user + ';pwd=' + this.password + ';Data Source=' + this.server);
    this.log.add('����������� ���������� � �� ' + this.name + ' �� ������� ' + this.server);
    this.connect_tries = 0;
    this.connection.CommandTimeout = 9000;
  } catch(e) {
    this.log.add(e.name + ' -> ' + e.message, 'warn');
    if (this.connect_tries < this.max_reconnects || this.max_reconnects == 0) {
      this.sleep(this.reconnect_interval);
      this.connect();
    } else {
      this.log.add('��������� ������� ���������� � �� ' + this.name + ' �� ������� ' + this.server + '. ����������� ������ ����������.', 'error');
    }
  }
  return this;
};

/**
 * ��������� ���������� � ��
 *
 * @returns {AISAutomation.db}
 */
AISAutomation.db.prototype.disconnect = function() {
  if (this.connection.State == 1) {
    this.connection.Close;
    this.log.add('���������� � �� ' + this.name + ' �� ������� ' + this.server + ' ���� �������.');
  }
  this.connection = null;
  return this;
};

/**
 * delay � �������������
 *
 * @param [delay] {integer} - ����� �����
 * @returns {AISAutomation.db}
 */
AISAutomation.db.prototype.sleep = function(delay) {
  var start_time = +new Date();
  var end_time = delay + +start_time;
  var current_time = null
  do {
    current_time = +new Date();
  } while (current_time <= end_time)
  return this;
};

/**
 * ��������� ������ � ��
 *
 * @param [strQuery] {string} - ������
 * @param [options] {object[]} - ������ �������� � ������������� ��������� ����������
 * @returns {ADODB.Recordset}
 */
AISAutomation.db.prototype.query = function(strQuery, options) {
  var options = options || {};
  var cursorType = options.cursorType || 0;
  var adLockReadOnly = options.adLockReadOnly || 1;
  var adCmdText = options.adCmdText || 1;

  try {
    this.connect_tries++;
    var result = new ActiveXObject('ADODB.Recordset');
    result.open(strQuery, this.connection, cursorType, adLockReadOnly, adCmdText);
    this.connect_tries = 0;
  } catch(e) {
    this.log.add(e.name + ' -> ' + e.message + ' ---> ' + strQuery, 'warn');
    if (this.connect_tries < this.max_reconnects || this.max_reconnects == 0) {
      this.sleep(this.reconnect_interval);
      this.query(strQuery);
    } else {
      this.disconnect();
      this.log.add('��������� ������� ���������� ������� ' + strQuery.replace(/ +/gi, ' ') + ' � ���� ������ ' + this.name + '. ����������� ������ ����������.', 'error');
    }
  }
  return result;
};

/**
 * ��������� (���� �� ����������) ���� ��������� (���� �����) ������ � �� � ���������� ID
 * �����:
 * id_column - �������, � ������� �������� ID
 * table - �������, �� ������� ������������� ������
 * condition - �������, �� ������� ����������� ������������� ������
 * values (������������� ������ ���� - ��������) - ������, ����������� � �������
 * select_if_exists (���������� ������ �����) - �������, ������ �� ������� ����� �����������, ���� � �� ��� ���� ������. ID ����� �� ���������, ������������� ����������.
 * need_to_be_updated ���������, ����� �� �������� ������, ���� ��� ����������
 *
 * @param [options] {object[]} - ������ �������� � ������������� ��������� ����������
 * @returns {ADODB.Recordset}
 */
AISAutomation.db.prototype.insertOrId = function(options) {
  options = options || {};
  options.id_column = options.id_column || 'Id';
  options.condition = options.condition || '1=1';

  var res_keys = new Array();
  var res_values = new Array();

  for (key in options.values) {
    res_keys.push(key);
    res_values.push(options.values[key]);
  }

  var res = this.query('\
    IF NOT EXISTS (SELECT [' + options.id_column + '] FROM [' + this.name + '].' + options.table + ' WHERE ' + options.condition + ')\
    BEGIN\
      INSERT INTO [' + this.name + '].' + options.table + '\
        (' + res_keys + ')\
      OUTPUT Inserted.' + options.id_column + '\
      VALUES (' + res_values + ')\
    END\
    ELSE\
    BEGIN\
      SELECT TOP 1 [' + options.id_column + ']' + (options.select_if_exists ? ', ' + options.select_if_exists : '') + ' FROM [' + this.name + '].' + options.table + ' WHERE ' + options.condition + '\
    END');

  var res_arr = {};

  // ������� ������������� ������ �� ����������
  res_arr[options.id_column] = String(res.Fields.Item(options.id_column));
  if (res.Fields.count > 1) {
    for (var i = 0; i < options.select_if_exists.length; i++)
      res_arr[options.select_if_exists[i]] = String(res.Fields.Item(options.select_if_exists[i]));
  }
  // TODO: ����������, ���� �������, ��� ������ ��������� ID.
  // ����� ���� ���������� ������� ������ ��� ������� - �������
  // ���������, � ����� ��� � ���������
  if (options.need_to_be_updated === true) {
    this.log.add('���������� ���������� � ������� ' + options.table + ' � ID ' + res_arr[options.id_column]);
    var update_new_values = '';
    for (var i = 0; i < res_keys.length; i++) {
      update_new_values += res_keys[i] + ' = ' + res_values[i];
      if (i < res_keys.length - 1) update_new_values += ', ';
    }
    this.query('\
      UPDATE [' + this.name + '].' + options.table + ' \
        SET ' + update_new_values + ' \
      WHERE ' + options.id_column + ' = ' + res_arr[options.id_column]);
  }

  return res_arr;
};

/**
 * ����������, �������� �� ���������� ������ null
 *
 * @param [obj] {ADODB.Recordset.Item} - ����������� ������
 * @returns {boolean}
 */
AISAutomation.db.isDbNull = function(obj) {
  obj = String(obj);
  return obj == "" || obj == null || obj == "null" || typeof obj == "undefined";
};

/**
 * ����������� ������ � ������(�)
 *
 * @param [src_str] {ADODB.Recordset.Item} - �������� ������
 * @param [left] {string} - ����� ����� �������
 * @param [right] {string} - ������ ����� �������
 * @returns {string}
 */
AISAutomation.db.wrap = function(src_str, left, right) {
  left = !left && left !== "" ? "'" : left;
  right = right || null;
  return left + src_str + (right || left);
};

/**
 * ����������� ������ � ������(�)
 *
 * @param [src_str] {ADODB.Recordset.Item} - �������� ������
 * @returns {string}
 */
AISAutomation.db.safe = function(src_str) {
  return src_str.replace(/\'/gi, "\"");
};

/**
 * ����������� ������ �������� � ���������
 *
 * @param [src_str] {ADODB.Recordset.Item} - �������� ������
 * @param [options] {object[]} - ������ �������� � ������������� ��������� ����������
 * @returns {string}
 */
AISAutomation.db.not_null = function(src_str, options) {
  options = options || {};
  src_str = String(src_str);
  var empty_src_str = this.isDbNull(src_str);
  var replace_str = options.replace_str || "";
  if (options.safe) {
    src_str = this.safe(src_str);
    replace_str = this.safe(replace_str); 
  }
  if (options.wrap) {
    src_str = this.wrap(src_str);
    replace_str = this.wrap(replace_str);
  }
  return empty_src_str ? replace_str : src_str;
};

/**
 * �������� ������ �� ������ �����
 *
 * @param [src_str] {string} - �������� ������
 * @param [dst_length] {integer} - ������ ����� ������
 * @returns {string}
 */
AISAutomation.db.cutToLength = function(src_str, dst_length) {
  dst_length = parseInt(dst_length);
  var dst_str = src_str;
  var terminator = " ...";
  if (src_str.length > dst_length) {
    dst_str = src_str.substr(0, dst_length - terminator.length) + terminator;
  }
  return dst_str;
};

/**
 * ��� (int(4)) ����������� � ���� (datetime).
 *
 * @param [year] {integer} - ���
 * @param [period] {integer} - ������: 0 - 1 ������, 1 - 31 �������
 * @returns {string}
 */
AISAutomation.db.yearToDate = function(year, period) {
  return year < 1000 || year > 9999 ? "NULL" : "CONVERT(datetime2, '" + year + "-" + (period == 0 ? "01-01" : "12-31") + " 00:00:00', 20)";
};

/**
 * ������������� 
 *
 * @param [table] {string} - ������� ��, � ������� ���������� �������� ������� col
 * @param [id] {integer} - Id ������, � ������� ���������� �������� ������� col
 * @param [col] {string} - �������, � ������� ����������� ����� ��������
 * @param [value] {any} - ����� ��������, ������������ � �������
 * @returns {AISAutomation.db}
 */
AISAutomation.db.prototype.changeColValue = function(tbl, id, col, val) {
  var new_value = {}; new_value[col] = val;
  var restriction = this.insertOrId({
    table: tbl,
    id_column: 'Id',
    condition: 'Id = ' + id,
    id_column: 'Id',
    values: new_value,
    need_to_be_updated: true
  });
};