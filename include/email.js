AISAutomation.email = {};

AISAutomation.email.send = function(email_config, email_recipient, subject, message) {
  // ���������� �������������� �� e-mail
  var objMail = new ActiveXObject('CDO.Message');
  var objConfig = new ActiveXObject('CDO.Configuration');
  var objFields = objConfig.Fields;
  with (objFields) {
    Item('http://schemas.microsoft.com/cdo/configuration/sendusing')= 2;
    Item('http://schemas.microsoft.com/cdo/configuration/smtpserver')= email_config.smtpserver;
    Item('http://schemas.microsoft.com/cdo/configuration/smtpserverport')= email_config.smtpserverport;
    Item('http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout') = email_config.smtpconnectiontimeout;
    Item('http://schemas.microsoft.com/cdo/configuration/smtpauthenticate') = 1;
    Item('http://schemas.microsoft.com/cdo/configuration/smtpusessl') = email_config.smtpusessl;
    Item('http://schemas.microsoft.com/cdo/configuration/sendusername') = email_config.sendusername;
    Item('http://schemas.microsoft.com/cdo/configuration/sendpassword') = email_config.sendpassword;
    Update();
  } 
  with (objMail) {
    Configuration = objConfig;
    To = email_recipient;
    From = email_config.fromfield;
    Subject = subject;
    TextBody = message;
    Send();
  }
};