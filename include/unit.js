/**
 * ������� ������ unit, ���������� ���������
 *
 * @param [options] {object[]} - ������ �������� � ������������� ��������� ����������
 * @returns {AISAutomation.unit}
 */
AISAutomation.unit = function (options) {
  var options = options || {};
  if (!AISAutomation.inventory.prototype.isPrototypeOf(options['inventory'])) throw new Error('� ������ AISAutomation.unit ������ ���� ������� �������� options.inventory ���� AISAutomation.inventory');
  this.inventory = options.inventory;
  this.info = {};
  this.restrictions = {};
  this.id = options['id'];
  this.info.archive_id = this.inventory.info.archive_id;
  this.info.inventory_id = this.inventory.info.id;
  this.inventory.fund.log.add('���� � Id ' + this.id + ' ������������', 'debug');
  return this;
};

/**
 * ������������ ��� �������� ���� �� ������� �� � ������ ��� �����
 *
 * @returns {integer}
 */
AISAutomation.unit.prototype.unit_media_id = function (med_af) {
  switch (med_af) {
    case 'T':
      this.info.media_type = 1;
      break;
    case 'E':
      this.info.media_type = 2;
      break;
    default:
      this.info.media_type = 1;
  }
  return this.info.media_type;
};

/**
 * ������������ �������� ���� �� ������� �� � ������ ��� �����
 *
 * @returns {integer}
 */
AISAutomation.unit.prototype.unit_value_id = function (val_af) {
  switch (val_af) {
    case 'a':
      this.info.value_id = 4;
      break;
    case 'b':
      this.info.value_id = 2;
      break;
    case 'c':
      this.info.value_id = 3;
      break;
    default:
      this.info.value_id = 1;
  }
  return this.info.value_id;
};

/**
 * ��������� ����� ���� � ��������� ����
 *
 * @returns {AISAutomation.unit}
 */
AISAutomation.unit.prototype.build_full_unit_number = function () {
  this.full_number = this.info.number + this.info.letter + (String(this.info.volume) == 'NULL' ? '' : ' �. ' + this.info.volume);
  return this;
};

/**
 * ��������� ���������, ��������� ������ ���� (��� �������)
 *
 * @returns {AISAutomation.unit}
 */
AISAutomation.unit.prototype.scan_storage = function () {
  this.inventory.fund.log.add('������������ ��������� ����������� �������. ����� ������� ����', 'debug');
  for (var storageID = 0; storageID < this.inventory.fund.storage.units_root.length; storageID++) {
    // ������ ��� �������� ���������� �� �������,
    // ������� ������ ��� ���� ������� �����,
    // ������� ��� �������
    this.inventory.fund.log.add("�������� ������ �� ������ ���� " + this.full_number + " ����� " + this.inventory.full_number + " ����� " + this.inventory.fund.full_number, "info");
    var clear_unit_images = this.inventory.fund.ais_db.query("DELETE FROM [tblStorage] WHERE ItemType = 5 AND ItemId = " + this.info.id);
    this.inventory.fund.log.add("����� ����������� ������� ���� " + this.full_number + " ����� " + this.inventory.full_number + " ����� " + this.inventory.fund.full_number + " � ��������� " + this.inventory.fund.storage.units_root[storageID], "info");

    var unit_storage = new AISAutomation.storage({
      log:              this.inventory.fund.log,
      root:             this.inventory.fund.storage.units_root[storageID],
      extensions:       this.inventory.fund.storage.extensions,
      fund_prefix:      this.inventory.fund.info.letter1,
      fund_number:      this.inventory.fund.info.number,
      fund_letter:      this.inventory.fund.info.letter2,
      inventory_number: this.inventory.info.number,
      inventory_letter: this.inventory.info.letter,
      inventory_volume: (this.inventory.fund.storage.ignore_inventories_volumes ? "" : this.inventory.info.volume),
      unit_number:      this.info.number,
      unit_letter:      this.info.letter
    });

    // �������� ������ ������
    var unit_images = unit_storage.get_files();
    // ���� ����� � ����� ����, �������, ��� ����� ���������
    // ��������������� �� ����� � �������� ������ �� ������
    if (unit_images.length > 0) {
      // ���� �� ��������� ��� ������ ������� ��������,
      // ����� ��������� ������ �������.
      var aiss_query = "";
      for (var image = 0; image < unit_images.length; image++) {
        this.inventory.fund.log.add("���������� ���������� � ����� " + unit_images[image]['path'] + " � ���� " + this.full_number + " ����� " + this.inventory.full_number + " ����� " + this.inventory.fund.full_number, "debug");
        // ������� ���������� � ����� ������ � �� ���. ���� ��� ��� ��� ����, ��������
        aiss_query += "INSERT INTO [dbo].[tblStorage]\
          (ItemId, ItemType, StorageType, Name, Path, TotalPages, Deleted, CreationTime, Hidden)\
          VALUES (" + this.info.id + ", 5, 1, " + AISAutomation.db.wrap(AISAutomation.db.safe(unit_images[image]['name'])) + ", " + AISAutomation.db.wrap(AISAutomation.db.safe(unit_images[image]['path'])) + ", 1, 0, CURRENT_TIMESTAMP, 0);";
        // var aiss = this.inventory.fund.ais_db.query("INSERT INTO [dbo].[tblStorage]\
        //   (ItemId, ItemType, StorageType, Name, Path, TotalPages, Deleted, CreationTime, Hidden)\
        //   VALUES (" + this.info.id + ", 5, 1, " + AISAutomation.db.wrap(AISAutomation.db.safe(unit_images[image]['name'])) + ", " + AISAutomation.db.wrap(AISAutomation.db.safe(unit_images[image]['path'])) + ", 1, 0, CURRENT_TIMESTAMP, 0)");
      }
      this.inventory.fund.log.add("���������� � �� ���������� �� ������� ���� " + this.full_number + " ����� " + this.inventory.full_number + " ����� " + this.inventory.fund.full_number, "info");
      var aiss = this.inventory.fund.ais_db.query(aiss_query);
      break;
    }
  }
  return this;
};

/**
 * ��������� ���� � ������ ������ ����������� �������. ���������� ID ������ � ������� ��������������� ������
 *
 * @returns {boolean}
 */
AISAutomation.unit.prototype.check_restriction = function(restriction_group, fund_res_id, inventory_res_id) {
  try {
    var units_list = this.inventory.fund.restrictions[restriction_group][fund_res_id]['inventories'][inventory_res_id]['units'] || [];
  } catch (e) {
    var units_list = [];
  }
  if (!Array.isArray(this.inventory.fund.restrictions[restriction_group])) this.inventory.fund.log.add('�������� restriction_group � ������ check_restriction ������� unit ������ ���� ������� � ���� �������', 'error');
  for (var i = 0; i < units_list.length; i++) {
    this.inventory.fund.log.add('�������� ������������ ����� ���� ' + this.full_number + ' ����� ' + this.inventory.full_number + ' ����� ' + this.inventory.fund.full_number + ' ����� ' + units_list[i]['unit_num'], 'debug');
    var unit_mask = new RegExp(units_list[i]['unit_num'], 'i');
    if (unit_mask.test(this.full_number)) {
      this.inventory.fund.log.add('���� ' + this.full_number + ' ����� ' + this.inventory.full_number + ' ����� ' + this.inventory.fund.full_number + ' �������� ��� ����� ' + units_list[i]['unit_num'] + ' � ������ ' + restriction_group, 'info');
      return true;
    }
  }
  return false;
};

/**
 * ���������� ���� � �����, ���� ��� ���� � ������
 *
 * @returns {AISAutomation.unit}
 */
AISAutomation.unit.prototype.show = function () {
  this.restrictions.show_unit = false;
  if (this.inventory.restrictions.show_all) {
    this.restrictions.show_unit = true;
  } else {
    if (this.inventory.restrictions.hide_id != -1) {
      this.restrictions.show_unit = this.check_restriction('show', this.inventory.fund.restrictions.hide_id, this.inventory.restrictions.hide_id);
    }
  }
  if (this.restrictions.show_unit) {
    this.inventory.fund.log.add('���� ' + this.full_number + ' ����� ' + this.inventory.full_number + ' ����� ' + this.inventory.fund.full_number + ' �� ����� �������� �������', 'info');
    this.inventory.fund.ais_db.changeColValue('[dbo].[tblArchiveFiles]', this.info.id, 'IsHidden', '0');
  }
  return this;
};

/**
 * �������� ���� � �����, ���� ��� ���� � ������
 *
 * @returns {AISAutomation.unit}
 */
AISAutomation.unit.prototype.hide = function () {
  this.restrictions.hide_unit = false;
  if (this.inventory.restrictions.hide_all) {
    this.restrictions.hide_unit = true;
  } else {
    if (this.inventory.restrictions.hide_id != -1) {
      this.restrictions.hide_unit = this.check_restriction('hide', this.inventory.fund.restrictions.hide_id, this.inventory.restrictions.hide_id);
    }
  }
  if (this.restrictions.hide_unit) {
    this.inventory.fund.log.add('���� ' + this.full_number + ' ����� ' + this.inventory.full_number + ' ����� ' + this.inventory.fund.full_number + ' ����� �������� �������', 'info');
    this.inventory.fund.ais_db.changeColValue('[dbo].[tblArchiveFiles]', this.info.id, 'IsHidden', '1');
  }
  return this;
};

/**
 * ��������, ��� ������ ���� ����� �������� ���� ��� ������, ���� ��� ���� � ������
 *
 * @returns {AISAutomation.unit}
 */
AISAutomation.unit.prototype.show_to_all = function () {
  this.restrictions.show_unit_for_all = false;
  if (this.inventory.restrictions.show_all_units) {
    this.restrictions.show_unit_for_all = true;
  } else {
    if (this.inventory.restrictions.show_to_all_id != -1) {
      this.restrictions.show_unit_for_all = this.check_restriction('show_to_all', this.inventory.fund.restrictions.show_to_all_id, this.inventory.restrictions.show_to_all_id);
    }
  }
  if (this.restrictions.show_unit_for_all) {
    this.inventory.fund.log.add('���� ' + this.full_number + ' ����� ' + this.inventory.full_number + ' ����� ' + this.inventory.fund.full_number + ' ����� �������� � ��� ����� ��� ������', 'info');
    this.inventory.fund.ais_db.changeColValue('[dbo].[tblArchiveFiles]', this.info.id, 'ShowToAll', '1');
  }
  return this;
};

/**
 * ��������� ������� ������/������� � ����
 *
 * @returns {AISAutomation.unit}
 */
AISAutomation.unit.prototype.apply_resctrictions = function () {
  switch (this.inventory.fund.restrictions.priority) {
    case 'show':
      this.show_to_all();
      this.hide();
      this.show();
      break;
    default:
      this.show_to_all();
      this.show();
      this.hide();
  }
  return this;
};

/**
 * ��������� ���������� �� ��������� ����� ���������� � ��� �����
 *
 * @returns {AISAutomation.unit}
 */
AISAutomation.unit.prototype.af_to_ais = function () {
  var is_hidden = (this.inventory.fund.allowed_isn_security_reason.indexOf(parseInt(this.isn_security_reason)) == -1 ? 1 : 0);
  var aisu = this.inventory.fund.ais_db.insertOrId({
    table: '[dbo].[tblArchiveFiles]',
    condition: '[Number] = ' + this.info.number +
      ' AND [Letter] LIKE ' + AISAutomation.db.wrap(this.info.letter) +
      ' AND [Volume]' + (this.info.volume == 'NULL' ? ' IS NULL ' : ' = ' + this.info.volume) +
      ' AND [InventoryId] = ' + this.info.inventory_id +
      ' AND [ArchiveId] = ' + this.info.archive_id,
    id_column: "Id",
    values: {
      InventoryId:          this.info.inventory_id,
      Name:                 AISAutomation.db.wrap(AISAutomation.db.safe(this.info.name)),
      Number:               this.info.number, 
      Letter:               AISAutomation.db.wrap(AISAutomation.db.safe(this.info.letter)),
      DocumentsStartDate:   this.info.documents_start_date,
      DocumentsLastDate:    this.info.documents_last_date,
      DocumentsQuantity:    0, 
      PagesQuantity:        this.info.pages_quantity, 
      DocumentationTypeId:  this.info.documentation_type_id,
      ValueId:              this.info.value_id,
      MediaType:            this.info.media_type,
      Rubric1:              AISAutomation.db.wrap(this.rubrics[0]),
      Rubric2:              AISAutomation.db.wrap(this.rubrics[1]),
      Rubric3:              AISAutomation.db.wrap(this.rubrics[2]),
      Rubric4:              AISAutomation.db.wrap(this.rubrics[3]),
      Remark:               AISAutomation.db.wrap(this.info.remark),
      ShowToAll:            0,
      CreationTime:         'CURRENT_TIMESTAMP',
      Deleted:              0,
      ArchiveId:            this.info.archive_id,
      ArchiveStorageId:     1,
      IsHidden:             is_hidden,
      Volume:               this.info.volume,
      Annotation:           AISAutomation.db.wrap(AISAutomation.db.safe(this.info.annotation))
    },
    need_to_be_updated: true
  });
  this.info.id = aisu['Id'];
  if (is_hidden == 1)
    this.inventory.fund.log.add('���� ����� ������ � �������� �������� �� ������� ����������� �������', 'warn');
  return this;
};

/**
 * ��������� ���������� � ��������� ����� �� ��������� �����
 *
 * @returns {string[]}
 */
AISAutomation.unit.prototype.build_inventory_structure = function (inv_cls, res) {
  if (!res) var res = [];
  if (inv_cls) {
    var inventory_structure_info = this.inventory.fund.sync_db.query("\
      SELECT NAME, ISN_HIGH_INVENTORY_CLS\
      FROM tblINVENTORY_STRUCTURE\
      WHERE ISNULL(FOREST_ELEM, '') NOT LIKE 'T' AND ISNULL(FOREST_ELEM, '') NOT LIKE 'F' \
      AND ISN_INVENTORY_CLS = " + inv_cls);
    if (inventory_structure_info.EOF) {
      // � ��������� ������ ���� ����� 4 ��������
      // ���� �������� ����� ������, � ��� ��������� 4 ������ ������
      // ������ ���������� � ������� splice
      res.push('', '', '', '');
      this.inventory.fund.log.add('��������� ��������� ����� ���� ' + this.full_number + ' ����� ' + this.inventory.full_number + ' ����� ' + this.inventory.fund.full_number, 'debug');
      return res.splice(0, 4);
    } else {
      var name = AISAutomation.db.not_null(inventory_structure_info.Fields.Item('NAME'));
      var high_cls = AISAutomation.db.not_null(inventory_structure_info.Fields.Item('ISN_HIGH_INVENTORY_CLS'), { replace_str: 0 });
      res.push(name);
      return this.build_inventory_structure(high_cls, res);
    }
  } else {
    res.push('', '', '', '');
    return res.splice(0, 4);
  }
};

/**
 * ��������� ���������� ��  ����� �� ��������� �����
 *
 * @returns {AISAutomation.unit}
 */
AISAutomation.unit.prototype.load_from_af = function () {
  var unit_info = this.inventory.fund.sync_db.query('\
    SELECT TOP 1\
      afu.NAME\
      ,afu.UNIT_NUM_1\
      ,afu.UNIT_NUM_2\
      ,afu.START_YEAR\
      ,afu.END_YEAR\
      ,afu.PAGE_COUNT\
      ,afu.ISN_DOC_TYPE\
      ,afu.UNIT_CATEGORY\
      ,afu.MEDIUM_TYPE\
      ,afu.NOTE\
      ,afu.VOL_NUM\
      ,afu.ANNOTATE\
      ,afu.ISN_INVENTORY_CLS\
      ,afu.ISN_SECURITY_REASON\
    FROM [' + this.inventory.fund.sync_db.name + '].[dbo].[tblUNIT] as afu\
    WHERE\
      afu.ISN_UNIT = ' + this.id);
  if (!unit_info.EOF) {
    this.info.name = AISAutomation.db.not_null(unit_info.Fields.Item('NAME'), { replace_str: '[��� ��������]' });
    this.info.number = AISAutomation.db.not_null(unit_info.Fields.Item('UNIT_NUM_1'), { replace_str: '0' });
    this.info.letter = AISAutomation.db.not_null(unit_info.Fields.Item('UNIT_NUM_2'));
    this.info.documents_start_date = AISAutomation.db.yearToDate(AISAutomation.db.not_null(unit_info.Fields.Item('START_YEAR'), { replace_str: 0 }), 0);
    this.info.documents_last_date = AISAutomation.db.yearToDate(AISAutomation.db.not_null(unit_info.Fields.Item('END_YEAR'), { replace_str: 0 }), 0);
    this.info.pages_quantity = AISAutomation.db.not_null(unit_info.Fields.Item('PAGE_COUNT'), { replace_str: '0' });
    this.info.documentation_type_id = this.inventory.fund.documentation_type_id(AISAutomation.db.not_null(unit_info.Fields.Item('ISN_DOC_TYPE'), { replace_str: 0 }));
    this.info.value_id = this.unit_value_id(AISAutomation.db.not_null(unit_info.Fields.Item('UNIT_CATEGORY'), { replace_str: 0 }));
    this.info.media_type = this.unit_media_id(AISAutomation.db.not_null(unit_info.Fields.Item('MEDIUM_TYPE'), { replace_str: 0 }));
    this.info.remark = AISAutomation.db.not_null(unit_info.Fields.Item('NOTE'));
    this.info.volume = AISAutomation.db.not_null(unit_info.Fields.Item('VOL_NUM'), { replace_str: 'NULL' });
    this.info.annotation = AISAutomation.db.not_null(unit_info.Fields.Item('ANNOTATE'));
    this.isn_inventory_cls = AISAutomation.db.not_null(unit_info.Fields.Item('ISN_INVENTORY_CLS'), { replace_str: '0' });
    this.isn_security_reason = AISAutomation.db.not_null(unit_info.Fields.Item('ISN_SECURITY_REASON'), { replace_str: '0' });
    this.build_full_unit_number();
    this.inventory.fund.log.add('���������� � ���� ' + this.full_number + ' ����� ' + this.inventory.full_number + ' ����� ' + this.inventory.fund.full_number + ' �������� �� �� ��', 'info');
    this.rubrics = this.build_inventory_structure(this.isn_inventory_cls);
  }
  return this;
};