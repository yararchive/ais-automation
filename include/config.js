AISAutomation.config = {
  operations: {
    sync: {
      archives: {
        gayo: {
          log: {
            by_date: true,
            unique_name: true,
            dir: '.\\log\\sync',
            file: 'af5-ais-gayo.log',
            level: ['info', 'warn', 'debug', 'error']
          },
          db: {
            server: 'DB_SERVER_NAME',
            name: 'DB_NAME',
            user: 'DB_USER_NAME',
            password: 'DB_USER_PASSWORD',
            max_reconnects: 0,
            reconnect_interval: 1000
          },
          funds_rubric: '�������� ������',
          storage: {
            units_root: ["\\\\gayofiler01\\gayo\\GAYO\\u", "\\\\gayofiler03\\vol_gayo\\GAYO\\u\\"],
            extensions: ["jpg", "png"]
          }
        }
      }
    }
  }
};