/**
 * ������� ������ inventory, ���������� ���������
 *
 * @param [options] {object[]} - ������ �������� � ������������� ��������� ����������
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory = function (options) {
  var options = options || {};
  if (!AISAutomation.fund.prototype.isPrototypeOf(options['fund'])) throw new Error('� ������ AISAutomation.inventory ������ ���� ������� �������� options.fund ���� AISAutomation.fund');
  this.fund = options.fund;
  this.info = {};
  this.restrictions = {};
  this.id = options['id'];
  this.info.archive_id = this.fund.info.archive_id;
  this.info.fund_id = this.fund.info.id;
  this.fund.log.add('����� � Id ' + this.id + ' ������������', 'debug');
  return this;
};

/**
 * ��������� ���������� ��� � �����
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.recalc_units = function () {
  var updated_unit = this.fund.ais_db.insertOrId({
    table: '[dbo].[tblInventories]',
    condition: '[Id] = ' + this.info.id,
    id_column: 'Id',
    values: {
      ArchiveFilesQuantity: '(SELECT COUNT(*)\
        FROM [tblArchiveFiles]\
        WHERE\
          Deleted = 0\
          AND InventoryId = ' + this.info.id + ')'
    },
    select_if_exists: ['ArchiveFilesQuantity'],
    need_to_be_updated: true
  });
  this.fund.log.add('���������� ���������� ��� � ����� ' + this.full_number + ' ����� ' + this.fund.full_number, 'info');
  return this;
};

/**
 * ������������ ����, ����������� � ������ �����
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.process_units = function () {
  this.fund.log.add('������ ��������� ��� ����� ' + this.full_number + ' ����� ' + this.fund.full_number, 'info');
  var current_unit_seq = 0;
  do {
    current_unit_seq++;

    var afu = this.fund.sync_db.query("\
      ;WITH UnitsNumbers AS (\
        SELECT\
          ISN_UNIT,\
          ROW_NUMBER() OVER(ORDER BY ISN_UNIT) AS seq,\
          ROW_NUMBER() OVER(ORDER BY ISN_UNIT DESC) AS totrows\
        FROM [" + this.fund.sync_db.name + "].[dbo].[tblUNIT]\
        WHERE\
          Deleted = 0\
          AND ISNULL([ISN_SECURLEVEL], 0) <> 2\
          AND ISNULL([IS_LOST], '') NOT LIKE 'Y'\
          AND [ISN_INVENTORY] = " + this.id + "\
      )\
      SELECT\
        ISN_UNIT\
      FROM UnitsNumbers\
      WHERE seq = " + current_unit_seq);

    if (!afu.EOF) {
      var unit = new AISAutomation.unit({
        'inventory': this,
        'id': parseInt(AISAutomation.db.not_null(afu.Fields.Item('ISN_UNIT')))
      });

      unit.load_from_af();
      unit.af_to_ais();
      unit.apply_resctrictions();
      unit.scan_storage();
    }
  } while (!afu.EOF)
  this.fund.log.add('��������� ��������� ��� ����� ' + this.full_number + ' ����� ' + this.fund.full_number, 'info');
  return this;
};

/**
 * ������� �� ����� ����, ������� ��� � �������� �����
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.clear_units = function () {

  try {
    var need_clear_units = AISAutomation.cli.getOptionValue('--clear_units');
  } catch (ex) {
    var need_clear_units = false;
  }
  if (need_clear_units) {
    this.fund.log.add('�������� ������ ������ �������� �� ����� ' + this.full_number + ' ����� ' + this.fund.full_number, 'info');
    this.fund.ais_db.query("\
      UPDATE [" + this.fund.ais_db.name + "].[dbo].[tblArchiveFiles]\
      SET\
        Deleted = 1\
      WHERE\
        Id IN (\
          SELECT aisu.Id\
          FROM [" + this.fund.ais_db.name + "].[dbo].[tblArchiveFiles] aisu\
          WHERE\
            aisu.InventoryId = " + this.info.id + "\
            AND aisu.Deleted = 0\
            AND NOT EXISTS (\
              SELECT afu.ID\
              FROM [" + this.fund.sync_db.name + "].[dbo].[tblUNIT] afu\
              WHERE\
                ISNULL(afu.UNIT_NUM_1, '') LIKE ISNULL(aisu.Number, '')\
                AND ISNULL(afu.UNIT_NUM_2, '') LIKE ISNULL(aisu.Letter, '')\
                AND ISNULL(afu.VOL_NUM, 0) = ISNULL(aisu.Volume, 0)\
                AND afu.Deleted = 0\
                AND ISNULL(afu.IS_LOST, '') NOT LIKE 'Y'\
                AND ISNULL(afu.ISN_SECURLEVEL, 0) <> 2\
                AND afu.ISN_INVENTORY IN (\
                  SELECT afi.ISN_INVENTORY\
                  FROM [" + this.fund.sync_db.name + "].[dbo].[tblINVENTORY] afi\
                  WHERE\
                    ISNULL(afi.INVENTORY_NUM_1, '') LIKE '" + this.info.number + "'\
                    AND ISNULL(afi.INVENTORY_NUM_2, '') LIKE '" + this.info.letter + "'\
                    AND afi.ISN_FUND = " + this.fund.id + "\
                )\
            )\
        )");
  }
  return this;
};

/**
 * ��������� ���������, ��������� ������ ����� (��� �������)
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.scan_storage = function () {
  this.fund.log.add('������������ ��������� ����������� �������. ����� ������� �����', 'debug');
  for (var storageID = 0; storageID < this.fund.storage.inventories_root.length; storageID++) {
    // ������ ��� �������� ���������� �� �������,
    // ������� ������ ��� ���� ������� �����,
    // ������� ��� �������
    this.fund.log.add("�������� ������ �� ������ ����� " + this.full_number + " ����� " + this.fund.full_number, "info");
    var clear_inventory_images = this.fund.ais_db.query("DELETE FROM [tblStorage] WHERE ItemType = 4 AND ItemId = " + this.info.id);
    this.fund.log.add("����� ����������� ������� ����� " + this.full_number + " ����� " + this.fund.full_number + " � ��������� " + this.fund.storage.inventories_root[storageID], "info");

    var inventory_storage = new AISAutomation.storage({
      log:              this.fund.log,
      root:             this.fund.storage.inventories_root[storageID],
      extensions:       this.fund.storage.extensions,
      fund_prefix:      this.fund.info.letter1,
      fund_number:      this.fund.info.number,
      fund_letter:      this.fund.info.letter2,
      inventory_number: this.info.number,
      inventory_letter: this.info.letter,
      inventory_volume: (this.fund.storage.ignore_inventories_volumes ? "" : this.info.volume)
    });

    // �������� ������ ������
    var inventory_images = inventory_storage.get_files();
    // ���� ����� � ����� ����, �������, ��� ����� ���������
    // ��������������� �� ����� � �������� ������ �� ������
    if (inventory_images.length > 0) {
      // ���� �� ��������� ��� ������ ������� ��������,
      // ����� ��������� ������ �������.
      for (var image = 0; image < inventory_images.length; image++) {
        this.fund.log.add("���������� ���������� � ����� " + inventory_images[image]['path'] + " � ����� " + this.full_number + " ����� " + this.fund.full_number, "debug");
        // ������� ���������� � ����� ������ � �� ���. ���� ��� ��� ��� ����, ��������
        var aiss = this.fund.ais_db.query("INSERT INTO [dbo].[tblStorage]\
          (ItemId, ItemType, StorageType, Name, Path, TotalPages, Deleted, CreationTime, Hidden)\
          VALUES (" + this.info.id + ", 4, 1, " + AISAutomation.db.wrap(AISAutomation.db.safe(inventory_images[image]['name'])) + ", " + AISAutomation.db.wrap(AISAutomation.db.safe(inventory_images[image]['path'])) + ", 1, 0, CURRENT_TIMESTAMP, 0)");
      }
      break;
    }
  }
  return this;
};

/**
 * ��������� ����� ����� � ��������� ����
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.build_full_inventory_number = function () {
  this.full_number = this.info.number + this.info.letter;
  if (!this.fund.ignore_inventories_volumes)
    this.full_number += (String(this.info.volume) ? ' �. ' + this.info.volume : '');
  return this;
};

/**
 * ��������� ����� � ������ ������ ����������� �������. ���������� ID ������ � ������� ��������������� ������
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.check_restriction = function(restriction_group, fund_res_id) {
  var inventories_list = this.fund.restrictions[restriction_group][fund_res_id]['inventories'];
  if (!Array.isArray(this.fund.restrictions[restriction_group])) this.fund.log.add('�������� restriction_group � ������ check_restriction ������� inventory ������ ���� ������� � ���� �������', 'error');
  for (var i = 0; i < inventories_list.length; i++) {
    this.fund.log.add('�������� ������������ ����� ����� ' + this.full_number + ' ����� ' + this.fund.full_number + ' ����� ' + inventories_list[i]['inventory_num'], 'debug');
    var inventory_mask = new RegExp(inventories_list[i]['inventory_num'], 'i');
    if (inventory_mask.test(this.full_number)) {
      this.fund.log.add('����� ' + this.full_number + ' ����� ' + this.fund.full_number + ' �������� ��� ����� ' + inventories_list[i]['inventory_num'] + ' � ������ ' + restriction_group, 'info');
      this.fund.log.add('����������� ������������� �������/������ ���� ����� ' + this.full_number + ' ����� ' + this.fund.full_number + ' �� ��������� ��������', 'debug');
      if (Array.isArray(inventories_list[i]['units'])) {
        var whole_inventory = false;
      } else {
        var whole_inventory = true;
      }
      return [i, whole_inventory];
    }
  }
  return [-1, false];
};

/**
 * �������� ����� � �����, ���� ��� ���� � ������ ������ ��� �������
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.hide = function () {
  this.restrictions.hide_id = -1;
  this.restrictions.hide_all = false;
  if (this.fund.restrictions.hide_all) {
    this.restrictions.hide_all = true;
  } else {
    if (this.fund.restrictions.hide_id != -1) {
      var res_check = this.check_restriction('hide', this.fund.restrictions.hide_id);
      this.restrictions.hide_id = res_check[0];
      this.restrictions.hide_all = res_check[1];
    }
  }
  if (this.restrictions.hide_all) {
    this.fund.log.add('����� ' + this.full_number + ' ����� ' + this.fund.full_number + ' ����� ������ � ��� �����', 'info');
    this.fund.ais_db.changeColValue('[dbo].[tblInventories]', this.info.id, 'IsHidden', '1');
  }
  return this;
};

/**
 * ���������� ����� � �����, ���� ��� ���� � ������ ������ ��� ������
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.show = function () {
  this.restrictions.show_id = -1;
  this.restrictions.show_all = false;
  if (this.fund.restrictions.show_all) {
    this.restrictions.show_all = true;
  } else {
    if (this.fund.restrictions.show_id != -1) {
      var res_check = this.check_restriction('show', this.fund.restrictions.show_id);
      this.restrictions.show_id = res_check[0];
      this.restrictions.show_all = res_check[1];
    }
  }
  if (this.restrictions.show_all) {
    this.fund.log.add('����� ' + this.full_number + ' ����� ' + this.fund.full_number + ' ����� �������� � ��� �����', 'info');
    this.fund.ais_db.changeColValue('[dbo].[tblInventories]', this.info.id, 'IsHidden', '0');
  }
  return this;
};

/**
 * ��������, ��� ������ ��� ����� ����� �������� ���� ��� ������, ���� ��� ���� � ������ ������ � ��������� ������
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.show_to_all = function () {
  this.restrictions.show_to_all_id = -1;
  this.restrictions.show_all_units = false;
  if (this.fund.restrictions.show_all_units) {
    this.restrictions.show_all_units = true;
  } else {
    if (this.fund.restrictions.show_to_all_id != -1) {
      var res_check = this.check_restriction('show_to_all', this.fund.restrictions.show_to_all_id);
      this.restrictions.show_to_all_id = res_check[0];
      this.restrictions.show_all_units = res_check[1];
    }
  }
  if (this.restrictions.show_all_units) {
    this.fund.log.add('���� ����� ' + this.full_number + ' ����� ' + this.fund.full_number + ' ����� �������� � ��� ����� ��� ������', 'info');
  }
  return this;
};

/**
 * ��������� ������� ������/������� � �����
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.apply_resctrictions = function () {
  switch (this.fund.restrictions.priority) {
    case 'show':
      this.show_to_all();
      this.hide();
      this.show();
      break;
    default:
      this.show_to_all();
      this.show();
      this.hide();
  }
  return this;
};

/**
 * ��������� ���������� �� ��������� ����� ���������� � ��� �����
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.af_to_ais = function () {
  var is_hidden = (this.fund.allowed_isn_security_reason.indexOf(parseInt(this.isn_security_reason)) == -1 ? 1 : 0);
  var aisi = this.fund.ais_db.insertOrId({
    table: '[dbo].[tblInventories]',
    condition: '[Number] = ' + this.info.number +
      ' AND [Letter] LIKE ' + AISAutomation.db.wrap(this.info.letter) +
      ' AND [Volume]' + (this.fund.ignore_inventories_volumes || AISAutomation.db.isDbNull(this.info.volume) ? ' IS NULL ' : ' = ' + this.info.volume) +
      ' AND [FundId] = ' + this.info.fund_id +
      ' AND [ArchiveId] = ' + this.info.archive_id,
    id_column: "Id",
    values: {
      FundId:               this.info.fund_id,
      Name:                 AISAutomation.db.wrap(AISAutomation.db.safe(this.info.name)),
      Number:               this.info.number, 
      Letter:               AISAutomation.db.wrap(AISAutomation.db.safe(this.info.letter)),
      DocumentsStartDate:   this.info.documents_start_date,
      DocumentsLastDate:    this.info.documents_last_date,
      ArchiveFilesQuantity: 0, 
      DocumentationTypeId:  this.info.documentation_type_id,
      Annotation:           AISAutomation.db.wrap(AISAutomation.db.safe(this.info.annotation)),
      CreationTime:         'CURRENT_TIMESTAMP',
      Deleted:              0,
      ArchiveId:            this.info.archive_id,
      IsHidden:             is_hidden,
      Volume:               (AISAutomation.db.isDbNull(this.info.volume) ? 'NULL' : this.info.volume)
    },
    need_to_be_updated: true
  });
  this.info.id = aisi['Id'];
  if (is_hidden == 1)
    this.fund.log.add('����� ����� ������ � �������� �������� �� ������� ����������� �������', 'warn');
  return this;
};

/**
 * ��������� ���������� ��  ����� �� ��� �����
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.load_from_ais = function () {
  var inventory_info = this.fund.ais_db.query('\
    SELECT TOP 1\
      aisi.Name\
      ,aisi.Number\
      ,aisi.Letter\
      ,aisi.Volume\
      ,aisi.DocumentsStartDate\
      ,aisi.DocumentsLastDate\
      ,aisi.DocumentationTypeId\
      ,aisi.Annotation\
    FROM [' + this.fund.ais_db.name + '].[dbo].[tblInventories] as aisi\
    WHERE\
      aisi.Id = ' + this.id);
  if (!inventory_info.EOF) {
    this.info.name = AISAutomation.db.not_null(inventory_info.Fields.Item('Name'), { replace_str: '[��� ��������]' });
    this.info.number = AISAutomation.db.not_null(inventory_info.Fields.Item('Number'), { replace_str: '0' });
    this.info.letter = AISAutomation.db.not_null(inventory_info.Fields.Item('Letter'));
    this.info.documents_start_date = AISAutomation.db.wrap(AISAutomation.db.not_null(inventory_info.Fields.Item('DocumentsStartDate'), { replace_str: '' }));
    this.info.documents_last_date = AISAutomation.db.wrap(AISAutomation.db.not_null(inventory_info.Fields.Item('DocumentsLastDate'), { replace_str: '' }));
    this.info.documentation_type_id = AISAutomation.db.not_null(inventory_info.Fields.Item('DocumentationTypeId'), { replace_str: 0 });
    this.info.annotation = AISAutomation.db.not_null(inventory_info.Fields.Item('Annotation'));
    if (this.fund.ignore_inventories_volumes) {
      this.fund.log.add('��� ����� (��� �������) ����� ��������������', 'debug');
      this.info.volume = 'NULL';
    } else {
      this.info.volume = AISAutomation.db.not_null(inventory_info.Fields.Item('Volume'));
    }
    this.build_full_inventory_number();
    this.fund.log.add('���������� �� ����� ' + this.full_number + ' ����� ' + this.fund.full_number + ' �������� �� �� ��� �����', 'info');
  }
  return this;
};

/**
 * ��������� ���������� ��  ����� �� ��������� �����
 *
 * @returns {AISAutomation.inventory}
 */
AISAutomation.inventory.prototype.load_from_af = function () {
  var inventory_info = this.fund.sync_db.query('\
    SELECT TOP 1\
      afi.INVENTORY_NAME\
      ,afi.INVENTORY_NUM_1\
      ,afi.INVENTORY_NUM_2\
      ,afi.INVENTORY_NUM_3\
      ,afi.DOC_START_YEAR\
      ,afi.DOC_END_YEAR\
      ,afi.ISN_INVENTORY_TYPE\
      ,afi.ANNOTATE\
      ,afi.ISN_SECURITY_REASON\
    FROM [' + this.fund.sync_db.name + '].[dbo].[tblINVENTORY] as afi\
    WHERE\
      afi.ISN_INVENTORY = ' + this.id);
  if (!inventory_info.EOF) {
    this.info.name = AISAutomation.db.not_null(inventory_info.Fields.Item('INVENTORY_NAME'), { replace_str: '[��� ��������]' });
    this.info.number = AISAutomation.db.not_null(inventory_info.Fields.Item('INVENTORY_NUM_1'), { replace_str: '0' });
    this.info.letter = AISAutomation.db.not_null(inventory_info.Fields.Item('INVENTORY_NUM_2'));
    this.info.documents_start_date = AISAutomation.db.yearToDate(AISAutomation.db.not_null(inventory_info.Fields.Item('DOC_START_YEAR'), { replace_str: 0 }), 0);
    this.info.documents_last_date = AISAutomation.db.yearToDate(AISAutomation.db.not_null(inventory_info.Fields.Item('DOC_END_YEAR'), { replace_str: 0 }), 0);
    this.info.documentation_type_id = this.fund.documentation_type_id(AISAutomation.db.not_null(inventory_info.Fields.Item('ISN_INVENTORY_TYPE'), { replace_str: 0 }));
    this.info.annotation = AISAutomation.db.not_null(inventory_info.Fields.Item('ANNOTATE'));
    this.isn_security_reason = AISAutomation.db.not_null(inventory_info.Fields.Item('ISN_SECURITY_REASON'), { replace_str: '0' });
    if (this.fund.ignore_inventories_volumes) {
      this.fund.log.add('��� ����� (��� �������) ����� ��������������', 'debug');
      this.info.volume = 'NULL';
    } else {
      this.info.volume = AISAutomation.db.not_null(inventory_info.Fields.Item('INVENTORY_NUM_3'));
    }
    this.build_full_inventory_number();
    this.fund.log.add('���������� �� ����� ' + this.full_number + ' ����� ' + this.fund.full_number + ' �������� �� �� ��', 'info');
  }
  return this;
};