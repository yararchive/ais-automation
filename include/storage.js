AISAutomation.storage = function(options) {
  var options = options || {};
  if (!AISAutomation.log.prototype.isPrototypeOf(options.log)) throw new Error('� ������ AISAutomation.storage ������ ���� ������� �������� log ���� AISAutomation.log');
  this.log = options.log;
  // ���������� ��������� �������
  if (!options.root) this.log.add("���� � ��������� ������� �� �����, ������������ ������� �������", "warn");
  this.root = options.root || ".\\";
  
  this.extensions = options.extensions || ["jpg"];
  this.extensions_regex = null;

  this.fund_prefix = options.fund_prefix || "";
  this.fund_number = options.fund_number || "";
  this.fund_letter = options.fund_letter || "";

  this.inventory_number = options.inventory_number || "";
  this.inventory_letter = options.inventory_letter || "";
  this.inventory_volume = options.inventory_volume || "";

  this.unit_number = options.unit_number || "";
  this.unit_letter = options.unit_letter || "";

  this.full_fund = null;
  this.rel_path_to_fund = null;
  this.abs_path_to_fund = null;
  this.full_inventory = null;
  this.rel_path_to_inventory = null;
  this.abs_path_to_inventory = null;
  this.full_unit = null;
  this.rel_path_to_unit = null;
  this.abs_path_to_unit = null;
  this.is_correct_details = null;

  this.fso = AISAutomation.fs.fso;
  this.fldr = null;
  this.files = null;

  // �������� ������������ ���������� �����
  this.check_details();
  // ������������� ������ ������ � ����
  this.calculate_paths();
  // ������� ����������
  this.set_dir();
};

// �������� ������������ ���������� ������ �����, �����
AISAutomation.storage.prototype.check_details = function(force) {
  // ������������� ��������, ���� ����������� �������������� ����������
  // ��� �������� ������� �� �����������
  if (force || this.is_correct_details === null) {
    if (this.fund_number) {
      this.is_correct_details = this.fund_number && this.inventory_number;
    } else {
      this.is_correct_details = this.unit_number && this.inventory_number;
    }
  }
  return this.is_correct_details;
};

// ������������� ���� � ���������� �����, ������ ����� �����
AISAutomation.storage.prototype.get_full_fund = AISAutomation.storage.prototype.get_rel_path_to_fund = function(force) {
  if (force || this.full_fund === null || this.rel_path_to_fund === null)
    this.full_fund = this.rel_path_to_fund = (this.fund_prefix ? this.fund_prefix + "-" : "") +
      this.fund_number + (this.fund_letter ? "_" + this.fund_letter : "")
  return this.full_fund;
};

// ���������� ���� � ���������� �����
AISAutomation.storage.prototype.get_abs_path_to_fund = function(force) {
  if (force || this.abs_path_to_fund === null)
    this.abs_path_to_fund = this.root + (this.fund_number ? "\\" : "") + this.get_rel_path_to_fund();
  return this.abs_path_to_fund;
};

// ������ ����� �����
AISAutomation.storage.prototype.get_full_inventory = function(force) {
  if (force || this.full_inventory === null)
    this.full_inventory = this.inventory_number + (this.inventory_letter ? "_" + this.inventory_letter : "")
       + (this.inventory_volume ? "_�" + this.inventory_volume : "");
  return this.full_inventory;
};

// ������������� ���� � ��������� �����
AISAutomation.storage.prototype.get_rel_path_to_inventory = function(force) {
  if (force || this.rel_path_to_inventory === null)
    this.rel_path_to_inventory = this.get_rel_path_to_fund() + (this.fund_number ? "\\" : "") + this.get_full_fund() + (this.fund_number ? "-" : "") + this.get_full_inventory();
  return this.rel_path_to_inventory;
};

// ���������� ���� � ���������� �����
AISAutomation.storage.prototype.get_abs_path_to_inventory = function(force) {
  if (force || this.abs_path_to_inventory === null)
    this.abs_path_to_inventory = this.get_abs_path_to_fund() + "\\" + this.get_full_fund() + (this.fund_number ? "-" : "") + this.get_full_inventory();
  return this.abs_path_to_inventory;
};

// ������ ����� ����
AISAutomation.storage.prototype.get_full_unit = function(force) {
  if (force || this.full_unit === null)
    this.full_unit = this.unit_number + (this.unit_letter ? "_" + this.unit_letter : "");
  return this.full_unit;
};

// ������������� ���� � ��������� ����
AISAutomation.storage.prototype.get_rel_path_to_unit = function(force) {
  if (force || this.rel_path_to_unit === null)
    this.rel_path_to_unit = this.get_rel_path_to_inventory() + "\\" + this.get_full_fund() + (this.fund_number ? "-" : "") +
      this.get_full_inventory() + "-" + this.get_full_unit();
  return this.rel_path_to_unit;
};

// ���������� ���� � ���������� ����
AISAutomation.storage.prototype.get_abs_path_to_unit = function(force) {
  if (force || this.abs_path_to_unit === null)
    this.abs_path_to_unit = this.get_abs_path_to_inventory() + "\\" + this.get_full_fund() + (this.fund_number ? "-" : "") +
      this.get_full_inventory() + "-" + this.get_full_unit();
  return this.abs_path_to_unit;
};

AISAutomation.storage.prototype.calculate_paths = function(force) {
  force = force || false;
  if (this.is_correct_details) {
    if (this.unit_number) {
      this.get_rel_path_to_unit(force);
      this.get_abs_path_to_unit(force);
    } else {
      this.get_rel_path_to_inventory(force);
      this.get_abs_path_to_inventory(force);
    }
  } else {
    this.log.add("������� ������ ��� ���������, ����� �����, ����� ��� ����: �. " + this.get_full_fund() + 
      " ��. " + this.get_full_inventory() + " �. " + this.get_full_unit(), "error");
  }
  return this;
};

AISAutomation.storage.prototype.extensions_to_regex = function(force) {
  if (force || this.extensions_regex === null)
    this.extensions_regex = new RegExp("\\.(" + this.extensions.join("|") + ")$", "i");
  return this.extensions_regex;
};

AISAutomation.storage.prototype.set_dir = function() {
  if (this.unit_number) {
    this.log.add("������ ���������� " + this.get_abs_path_to_unit());
  } else {
    this.log.add("������ ���������� " + this.get_abs_path_to_inventory());
  }
  try {
    if (this.unit_number) {
      this.fldr = this.fso.GetFolder(this.get_abs_path_to_unit());
    } else {
      this.fldr = this.fso.GetFolder(this.get_abs_path_to_inventory());
    }
  } catch(e) {
    if (this.unit_number) {
      this.log.add("�. " + this.get_full_fund() + " ��. " + this.get_full_inventory() + 
        " �. " + this.get_full_unit() + " �� ����� ����������� �������", "info");
    } else {
      this.log.add("�. " + this.get_full_fund() + " ��. " + this.get_full_inventory() + " �� ����� ����������� �������", "info");
    }
    this.fldr = null;
  }
};

AISAutomation.storage.prototype.get_files = function() {
  this.files = [];
  if (this.fldr !== null) {
    var files_list = new Enumerator(this.fldr.Files);
    for (;!files_list.atEnd(); files_list.moveNext()) {
      file = files_list.item();
      if (file.Name.match(this.extensions_to_regex())) {
        if (this.unit_number) {
          this.files.push({ path: file.Path, name: this.get_rel_path_to_unit() + "\\" + file.Name });
        } else {
          this.files.push({ path: file.Path, name: this.get_rel_path_to_inventory() + "\\" + file.Name });
        }
      }
    }
  }
  return this.files;
};