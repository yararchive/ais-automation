Array.prototype.indexOf = function (value) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == value) {
      return i;
    }
  }
  return -1;
};

Array.isArray = function (value) {
  return Object.prototype.toString.call(value) === '[object Array]';
};