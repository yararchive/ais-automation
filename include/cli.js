AISAutomation.cli = {};

AISAutomation.cli.argv = [];

// ��������� � �����, ������� ����� ���� ��������
AISAutomation.cli.options = [];

// �������� ���������� �����
AISAutomation.cli.values = {};

AISAutomation.cli.option = {
  short: new RegExp('^-([^\\d-])$'),
  long: new RegExp('^--(\\S+)'),
  longEq: new RegExp('^(--\\S+)=(.*)'),
  combined: new RegExp('^-([^\\d-]{2,})$'),
  isLong: function (arg) { return this.long.test(arg) || this.longEq.test(arg) },
  isLongEq: function (arg) { return this.longEq.test(arg) },
  isShort: function (arg) { return this.short.test(arg) },
  isCombined: function (arg) { return this.combined.test(arg) },
  isOption: function (arg) { return this.isLong(arg) || this.isShort(arg) || this.isCombined(arg) }
}

/**
 * �������� � ������ AISAutomation.cli.argv ���������� ��������� ��������� ������
 *
 * @returns {AISAutomation.cli}
 */
AISAutomation.cli.prepare = function() {
  for (var i = 0; i < WScript.Arguments.length; i++) {
    this.argv.push(WScript.Arguments.Item(i));
  }
  return this;
};

/**
 * �������� � ������ AISAutomation.cli.options ��������� ��������� ��������� ������
 *
 * @param [options] {object[]} - ������ �������� � ������������� ��������� ����������
 * @returns {AISAutomation.cli}
 */
AISAutomation.cli.addOptions = function(options) {
  if (Array.isArray(options)) {
    for (var i = 0; i < options.length; i++) this.options.push(options[i]);
  } else {
    throw new Error('� ����� cli.addOptions ����� �������� ������');
  }
  return this;
};

/**
 * ��������� ������� ���������� � ���������� ��������� ������ ����� � ������ �����������
 *
 * @param [option] {string} - ����������� �����
 * @returns {string}
 */
AISAutomation.cli.getOptionName = function (option) {
  for (var i = 0; i < this.options.length; i++) {
    if (this.options[i]['name'] === option || this.options[i]['alias'] === option) return this.options[i]['name'];
  }
  throw new Error('�������� ����� ' + option);
};

/**
 * ���������� ��� �����
 *
 * @param [option] {string} - ����������� �����
 * @returns {string}
 */
AISAutomation.cli.getOptionType = function (option) {
  for (var i = 0; i < this.options.length; i++) {
    if (this.options[i]['name'] === option || this.options[i]['alias'] === option)
      if (this.options[i]['type'] === 'arg' || this.options[i]['type'] === 'opt') return this.options[i]['type'];
  }
  throw new Error('�������� type ����� ' + option + ' ������ ���� ����� ���� arg, ���� opt');
};

/**
 * ����������, ����� �� ���������� ����� ��������� ��������
 *
 * @param [option] {string} - ����������� �����
 * @returns {boolean}
 */
AISAutomation.cli.isOptionMultiple = function (option) {
  for (var i = 0; i < this.options.length; i++) {
    if (this.options[i]['name'] === option || this.options[i]['alias'] === option) {
      if (typeof(this.options[i]['multiple']) != 'undefined') return true;
    }
  }
  return false;
};

/**
 * �������� �������� �����
 *
 * @param [option] {string} - ����������� �����
 * @returns {string[]}
 */
AISAutomation.cli.getOptionValue = function(option) {
  if (typeof(this.values[option]) != 'undefined') {
    if (this.isOptionMultiple(option)) {
      return this.values[option];
    } else {
      return this.values[option][0];
    }
  } else {
    throw new Error('����� ' + option + ' �� ������');
  }
};

/**
 * �������� �������� �����
 *
 * @param [option] {string} - ��� �����
 * @param [value] {string} - ������������� ��������
 * @returns {AISAutomation.cli}
 */
AISAutomation.cli.setOptionValue = function(option, value) {
  if (!Array.isArray(this.values[option])) {
    this.values[option] = [];
  } else {
    if (!this.isOptionMultiple(option)) throw new Error('����� ' + option + ' �� ��������� ���������� ��������� ��������');
  }
  if (this.getOptionType(option) == 'opt' && typeof(value) != 'boolean') throw new Error('����� ' + option + ' �� ��������� ���������� ���������, ����� true � false');
  this.values[option].push(value);
  return this;
};

/**
 * ��������� ������������ ��������� ���������� ��������� ������, ���������� �� ��������
 *
 * @returns {AISAutomation.cli}
 */
AISAutomation.cli.parse = function() {
  for (var current_arg_position = 0; current_arg_position < this.argv.length; current_arg_position++) {
    // ������� ����������, �������� �� ��������������� ������� ������
    if (this.option.isOption(this.argv[current_arg_position])) {
      if (this.option.isLongEq(this.argv[current_arg_position])) {
        var arg_name = this.argv[current_arg_position].substr(0, this.argv[current_arg_position].indexOf('='));
        var arg_value = this.argv[current_arg_position].substr(this.argv[current_arg_position].indexOf('=') + 1);
        this.setOptionValue(arg_name, arg_value);
      } else {
        var current_arg_name = this.getOptionName(this.argv[current_arg_position]);
        var current_arg_type = this.getOptionType(current_arg_name);
        if (current_arg_type == 'opt') this.setOptionValue(current_arg_name, true);
      }
    } else {
      if (typeof(current_arg_name) == 'undefined') throw new Error('�������� ' + this.argv[current_arg_position] + ' �� ��������� �� ����� �����');
      this.setOptionValue(current_arg_name, this.argv[current_arg_position]);
    }
  }
  return this;
};


AISAutomation.cli.prepare();