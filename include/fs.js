AISAutomation.fs = {};

AISAutomation.fs.fso = new ActiveXObject("Scripting.FileSystemObject");

/**
 * Рекурсивное создание директории
 *
 * @param [folder] {string} - Имя директории
 * @returns {AISAutomation.fs}
 */
AISAutomation.fs.createFolder = function(folder) {
  var parent_folder = this.fso.GetParentFolderName(folder);
  if (!this.fso.FolderExists(parent_folder)) {
    this.createFolder(parent_folder);
  }
  return this.fso.FolderExists(folder) ? this : this.fso.CreateFolder(folder);
};

/**
 * Чтение текстового файла в строку
 *
 * @param [src_file] {string} - Имя файла
 * @returns {string}
 */
AISAutomation.fs.textFileToString = function(src_file) {
  var text_file = AISAutomation.fs.fso.OpenTextFile(src_file, 1, false, -2);
  var contents = '';
  while(!text_file.AtEndOfStream) {
    contents += text_file.ReadLine();
  }
  text_file.Close();
  return contents;
};