/**
 * ������� ������ fund, ���������� ���������
 *
 * @param [options] {object[]} - ������ �������� � ������������� ��������� ����������
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund = function (options) {
  var options = options || {};
  if (!AISAutomation.log.prototype.isPrototypeOf(options['log'])) throw new Error('� ������ AISAutomation.fund ������ ���� ������� �������� options.log ���� AISAutomation.log');
  this.log = options.log;
  if (!AISAutomation.db.prototype.isPrototypeOf(options['ais_db'])) throw new Error('� ������ AISAutomation.fund ������ ���� ������� �������� options.ais_db ���� AISAutomation.db');
  this.ais_db = options.ais_db;
  this.info = {};
  if (!AISAutomation.db.prototype.isPrototypeOf(options['sync_db'])) throw new Error('� ������ AISAutomation.fund ������ ���� ������� �������� options.sync_db ���� AISAutomation.db');
  this.sync_db = options['sync_db'];
  if (options['archive_id'] === parseInt(options['archive_id'])) {
    this.info.archive_id = Math.abs(options['archive_id']);
  } else {
    throw new Error('�������� archive_id ������ ���� ����� ������');
  }
  if (options['rubric_id'] === parseInt(options['rubric_id'])) {
    this.info.rubric_id = Math.abs(options['rubric_id']);
  } else {
    throw new Error('�������� rubric_id ������ ���� ����� ������');
  }
  this.ignore_inventories_volumes = options.ignore_inventories_volumes;
  if (!Array.isArray(options.allowed_isn_security_reason)) throw new Error('�������� allowed_isn_security_reason ������ ���� �������� ����� �����');
  this.allowed_isn_security_reason = options.allowed_isn_security_reason;
  this.restrictions = options['restrictions'];
  this.storage = options['storage'];
  this.id = options['id'];
  this.log.add('���� � Id ' + this.id + ' �����������', 'debug');
  return this;
};

/**
 * ��������� ����� ����� � ��������� ����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.build_full_fund_number = function () {
  this.full_number = (String(this.info.letter1) ? this.info.letter1 + '-' : '') + this.info.number + (String(this.info.letter2) ? this.info.letter2 : '');
  return this;
};

/**
 * ���� ���������� � ����� � ��� �����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.find_id_in_ais = function () {
  if (!this.info.id) {
    var aisf = this.ais_db.query("\
      SELECT TOP 1\
        Id\
      FROM [" + this.ais_db.name + "].[dbo].[tblFunds]\
      WHERE\
        Letter1 LIKE " + AISAutomation.db.wrap(this.info.letter1) + "\
        AND Number = " + this.info.number + "\
        AND Letter2 LIKE " + AISAutomation.db.wrap(this.info.letter2) + "\
        AND Deleted = 0\
        AND ArchiveId = " + this.info.archive_id + "\
        AND RubricId = " + this.info.rubric_id);
    if (!aisf.EOF) {
      this.info.id = parseInt(AISAutomation.db.not_null(aisf.Fields.Item('Id')));
    } else {
      this.log.add('���� ' + this.full_number + ' �� ������ � �� ��� �����. ���������� ������������� ��', 'error');
    }
  }
  return this;
};

/**
 * ��������� ���������� � ����� �� ��� �����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.load_from_ais = function () {
  var fund_info = this.ais_db.query('\
    SELECT TOP 1\
      aisf.Name\
      ,aisf.ShortName\
      ,aisf.Letter1\
      ,aisf.Number\
      ,aisf.Letter2\
      ,aisf.DocumentsStartDate\
      ,aisf.DocumentsLastDate\
      ,aisf.HistoryInformation\
      ,aisf.Annotation\
      ,aisf.FundCategoryId\
      ,aisf.DocumentationTypeId\
      ,aisf.Remark\
    FROM [' + this.ais_db.name + '].[dbo].[tblFunds] as aisf\
    WHERE\
      aisf.Id = ' + this.id);
  if (!fund_info.EOF) {
    this.info.name = AISAutomation.db.not_null(fund_info.Fields.Item('Name'));
    this.info.short_name = AISAutomation.db.cutToLength(AISAutomation.db.not_null(fund_info.Fields.Item('ShortName')), 255);
    this.info.number = AISAutomation.db.not_null(fund_info.Fields.Item('Number'));
    this.info.letter1 = AISAutomation.db.not_null(fund_info.Fields.Item('Letter1'));
    this.info.letter2 = AISAutomation.db.not_null(fund_info.Fields.Item('Letter2'));
    this.info.documents_start_date = AISAutomation.db.wrap(AISAutomation.db.not_null(fund_info.Fields.Item('DocumentsStartDate'), { replace_str: '' }));
    this.info.documents_last_date = AISAutomation.db.wrap(AISAutomation.db.not_null(fund_info.Fields.Item('DocumentsLastDate'), { replace_str: '' }));
    this.info.history_information = AISAutomation.db.not_null(fund_info.Fields.Item('HistoryInformation'));
    this.info.annotation = AISAutomation.db.not_null(fund_info.Fields.Item('Annotation'));
    this.info.fund_category_id = AISAutomation.db.not_null(fund_info.Fields.Item('FundCategoryId'), { replace_str: 'NULL' });
    this.info.documentation_type_id = AISAutomation.db.not_null(fund_info.Fields.Item('DocumentationTypeId'), { replace_str: 0 });
    this.info.remark = AISAutomation.db.not_null(fund_info.Fields.Item('Remark'));
    this.build_full_fund_number();
    this.log.add('���������� � ����� ' + this.full_number + ' �������� �� �� ��� �����', 'info');
  }
  return this;
};

/**
 * ��������� ���������� � ����� �� ��������� �����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.load_from_af = function () {
  var fund_info = this.sync_db.query('\
    SELECT TOP 1\
      aff.FUND_NAME_FULL\
      ,aff.FUND_NAME_SHORT\
      ,aff.FUND_NUM_1\
      ,aff.FUND_NUM_2\
      ,aff.FUND_NUM_3\
      ,aff.DOC_START_YEAR\
      ,aff.DOC_END_YEAR\
      ,aff.FUND_HISTORY\
      ,aff.ANNOTATE\
      ,aff.FUND_CATEGORY\
      ,aff.ISN_DOC_TYPE\
      ,aff.NOTE\
    FROM [' + this.sync_db.name + '].[dbo].[tblFUND] as aff\
    WHERE\
      aff.ISN_FUND = ' + this.id);
  if (!fund_info.EOF) {
    this.info.name = AISAutomation.db.not_null(fund_info.Fields.Item('FUND_NAME_FULL'));
    this.info.short_name = AISAutomation.db.cutToLength(AISAutomation.db.not_null(fund_info.Fields.Item('FUND_NAME_SHORT')), 255);
    this.info.number = AISAutomation.db.not_null(fund_info.Fields.Item('FUND_NUM_2'));
    this.info.letter1 = AISAutomation.db.not_null(fund_info.Fields.Item('FUND_NUM_1'));
    this.info.letter2 = AISAutomation.db.not_null(fund_info.Fields.Item('FUND_NUM_3'));
    this.info.documents_start_date = AISAutomation.db.yearToDate(AISAutomation.db.not_null(fund_info.Fields.Item('DOC_START_YEAR'), { replace_str: 0 }), 0);
    this.info.documents_last_date = AISAutomation.db.yearToDate(AISAutomation.db.not_null(fund_info.Fields.Item('DOC_END_YEAR'), { replace_str: 0 }), 0);
    this.info.history_information = AISAutomation.db.not_null(fund_info.Fields.Item('FUND_HISTORY'));
    this.info.annotation = AISAutomation.db.not_null(fund_info.Fields.Item('ANNOTATE'));
    this.info.fund_category_id = this.fund_category_id(AISAutomation.db.not_null(fund_info.Fields.Item('FUND_CATEGORY')));
    this.info.documentation_type_id = this.documentation_type_id(AISAutomation.db.not_null(fund_info.Fields.Item('ISN_DOC_TYPE'), { replace_str: 0 }));
    this.info.remark = AISAutomation.db.not_null(fund_info.Fields.Item('NOTE'));
    this.build_full_fund_number();
    this.log.add('���������� � ����� ' + this.full_number + ' �������� �� �� ��', 'info');
  }
  return this;
};

/**
 * ������������ �����, ����������� � ������� �����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.process_inventories = function () {
  this.log.add('������ ��������� ������ ����� ' + this.full_number, 'info');
  var current_inventory_seq = 0;
  do {
    current_inventory_seq++;

    var afi = this.sync_db.query("\
      ;WITH InventoriesNumbers AS (\
        SELECT\
          ISN_INVENTORY,\
          ROW_NUMBER() OVER(ORDER BY ISN_INVENTORY) AS seq,\
          ROW_NUMBER() OVER(ORDER BY ISN_INVENTORY DESC) AS totrows\
        FROM [" + this.sync_db.name + "].[dbo].[tblINVENTORY]\
        WHERE\
          Deleted = 0\
          AND ISNULL([ISN_SECURLEVEL], 0) <> 2\
          AND ISNULL([PRESENCE_FLAG], '') NOT LIKE 'b'\
          AND [ISN_FUND] = " + this.id + "\
      )\
      SELECT\
        ISN_INVENTORY\
      FROM InventoriesNumbers\
      WHERE seq = " + current_inventory_seq);

    if (!afi.EOF) {
      var inventory = new AISAutomation.inventory({
        'fund': this,
        'id': parseInt(AISAutomation.db.not_null(afi.Fields.Item('ISN_INVENTORY')))
      });

      inventory.load_from_af();
      inventory.af_to_ais();
      inventory.apply_resctrictions();
      inventory.scan_storage();
      inventory.process_units();
      inventory.clear_units();
      inventory.recalc_units();
    }
  } while (!afi.EOF)
  this.log.add('��������� ��������� ������ ����� ' + this.full_number, 'info');
  return this;
};

/**
 * ������� �� ����� �����, ������� ��� � �������� �����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.clear_inventories = function () {
  this.log.add('�������� ������ ������ ����� ' + this.full_number, 'info');
  this.ais_db.query("\
    UPDATE [" + this.ais_db.name + "].[dbo].[tblInventories]\
    SET\
      Deleted = 1\
    WHERE\
      Id IN (\
        SELECT aisi.Id\
          FROM [" + this.ais_db.name + "].[dbo].[tblInventories] aisi\
          WHERE\
            aisi.FundId = " + this.info.id + "\
            AND aisi.Deleted = 0\
            AND NOT EXISTS (\
              SELECT afi.ID\
              FROM [" + this.sync_db.name + "].[dbo].[tblINVENTORY] afi\
              WHERE\
                ISNULL(afi.INVENTORY_NUM_1, 0) = ISNULL(aisi.Number, 0)\
                AND ISNULL(afi.INVENTORY_NUM_2, '') LIKE ISNULL(aisi.Letter, '')\
                " + (this.ignore_inventories_volumes ? "" : "AND ISNULL(afi.INVENTORY_NUM_3, 0) = ISNULL(aisi.Volume, 0)") + "\
                AND afi.Deleted = 0\
                AND ISNULL(afi.PRESENCE_FLAG, '') NOT LIKE 'b'\
                AND ISNULL(afi.ISN_SECURLEVEL, 0) <> 2\
                AND afi.ISN_FUND = " + this.id + "\
            )\
        )");
  return this;
};

/**
 * ��������� ���������� �� ��������� ����� ���������� � ��� �����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.af_to_ais = function () {
  var aisf = this.ais_db.insertOrId({
    table: '[dbo].[tblFunds]',
    condition: '[Number] = ' + this.info.number +
      ' AND [Letter1] LIKE ' + AISAutomation.db.wrap(this.info.letter1) +
      ' AND [Letter2] LIKE ' + AISAutomation.db.wrap(this.info.letter2) +
      ' AND [RubricId] = ' + AISAutomation.db.wrap(this.info.rubric_id) +
      ' AND [ArchiveId] = ' + this.info.archive_id,
    id_column: "Id",
    values: {
      RubricId:             this.info.rubric_id, 
      Name:                 AISAutomation.db.wrap(AISAutomation.db.safe(this.info.name)),
      ShortName:            AISAutomation.db.wrap(AISAutomation.db.safe(this.info.short_name)),
      Number:               this.info.number, 
      Letter1:              AISAutomation.db.wrap(AISAutomation.db.safe(this.info.letter1)),
      Letter2:              AISAutomation.db.wrap(AISAutomation.db.safe(this.info.letter2)),
      DocumentsStartDate:   this.info.documents_start_date,
      DocumentsLastDate:    this.info.documents_last_date,
      ArchiveFilesQuantity: 0, 
      InventoriesQuantity:  0, 
      HistoryInformation:   AISAutomation.db.wrap(AISAutomation.db.safe(this.info.history_information)),
      Annotation:           AISAutomation.db.wrap(AISAutomation.db.safe(this.info.annotation)),
      FundCategoryId:       this.info.fund_category_id,
      DocumentationTypeId:  this.info.documentation_type_id,
      Remark:               AISAutomation.db.wrap(AISAutomation.db.safe(this.info.remark)),
      CreationTime:         'CURRENT_TIMESTAMP',
      Deleted:              0,
      ArchiveId:            this.info.archive_id
    },
    need_to_be_updated: true
  });
  this.info.id = aisf['Id'];
  return this;
};

/**
 * ��������� ���� � ������ ������ ����������� �������. ���������� ID ������ � ������� ��������������� ������
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.check_restriction = function(restriction_group) {
  if (!Array.isArray(this.restrictions[restriction_group])) this.log.add('�������� restriction_group � ������ check_restriction ������� fund ������ ���� ������� � ���� �������', 'error');
  for (var i = 0; i < this.restrictions[restriction_group].length; i++) {
    this.log.add('�������� ������������ ����� ����� ' + this.full_number + ' ����� ' + this.restrictions[restriction_group][i]['fund_num'], 'debug');
    var fund_mask = new RegExp(this.restrictions[restriction_group][i]['fund_num'], 'i');
    if (fund_mask.test(this.full_number)) {
      this.log.add('���� ' + this.full_number + ' �������� ��� ����� ' + this.restrictions[restriction_group][i]['fund_num'] + ' � ������ ' + restriction_group, 'info');
      this.log.add('����������� ������������� �������/������ ����� ����� ' + this.full_number + ' �� ��������� ��������', 'debug');
      if (Array.isArray(this.restrictions[restriction_group][i]['inventories'])) {
        var whole_fund = false;
      } else {
        var whole_fund = true;
      }
      return [i, whole_fund];
    }
  }
  return [-1, false];
};

/**
 * �������� ���� ���������, ���� �� ���� � ������ ������ ��� �������
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.hide = function () {
  var res_check = this.check_restriction('hide');
  this.restrictions.hide_id = res_check[0];
  this.restrictions.hide_all = res_check[1];
  if (this.restrictions.hide_all) {
    this.log.add('���� ' + this.full_number + ' ����� ����� �� ��� ����� ���������', 'info');
    this.ais_db.changeColValue('[dbo].[tblFunds]', this.info.id, 'Deleted', '1');
  }
  return this;
};

/**
 * �������� ���� � �������, ���� �� ���� � ������ ������ ��� ������
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.show = function () {
  var res_check = this.check_restriction('show');
  this.restrictions.show_id = res_check[0];
  this.restrictions.show_all = res_check[1];
  if (this.restrictions.show_all) {
    this.log.add('���� ' + this.full_number + ' ����� �������� � ��� �����', 'info');
    this.ais_db.changeColValue('[dbo].[tblFunds]', this.info.id, 'Deleted', '0');
  }
  return this;
};

/**
 * ��������, ��� ������ ��� ����� ����� �������� ���� ��� ������, ���� �� ���� � ������ ������ � ��������� ������
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.show_to_all = function () {
  var res_check = this.check_restriction('show_to_all');
  this.restrictions.show_to_all_id = res_check[0];
  this.restrictions.show_all_units = res_check[1];
  if (this.restrictions.show_all_units) {
    this.log.add('���� ����� ' + this.full_number + ' ����� �������� � ��� ����� ��� ������', 'info');
  }
  return this;
};

/**
 * ��������� ������� ������/������� � �����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.apply_resctrictions = function () {
  switch (this.restrictions.priority) {
    case 'show':
      this.show_to_all();
      this.hide();
      this.show();
      break;
    default:
      this.show_to_all();
      this.show();
      this.hide();
  }
  return this;
};

/**
 * ��������� ���������� ������ � �����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.recalc_inventories = function () {
  var updated_inventory = this.ais_db.insertOrId({
    table: '[dbo].[tblFunds]',
    condition: '[Id] = ' + this.info.id,
    id_column: 'Id',
    values: {
      InventoriesQuantity: '(SELECT count(*) FROM \
       [tblInventories] WHERE Deleted = 0 AND FundId = ' + this.info.id + ')'
    },
    select_if_exists: ['InventoriesQuantity'],
    need_to_be_updated: true
  });
  this.log.add('���������� ���������� ������ � ����� ' + this.full_number, 'info');
  return this;
};

/**
 * ��������� ���������� ��� � �����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.recalc_units = function () {
  var updated_unit = this.ais_db.insertOrId({
    table: '[dbo].[tblFunds]',
    condition: '[Id] = ' + this.info.id,
    id_column: 'Id',
    values: {
      ArchiveFilesQuantity: '(SELECT COUNT(*)\
        FROM [tblArchiveFiles] aisu\
        INNER JOIN [tblInventories] aisi ON\
          aisi.Id = aisu.InventoryId\
        WHERE\
          aisu.Deleted = 0\
          AND aisi.FundId = ' + this.info.id + ')'
    },
    select_if_exists: ['ArchiveFilesQuantity'],
    need_to_be_updated: true
  });
  this.log.add('���������� ���������� ��� � ����� ' + this.full_number, 'info');
  return this;
};

/**
 * ������������ ��������� ����� �� ������� �� � ������ ��� �����
 *
 * @returns {integer|null}
 */
AISAutomation.fund.prototype.fund_category_id = function (cat_af) {
  switch (cat_af) {
    case 'a':
      this.info.fund_category_id = 1;
      break;
    case 'b':
      this.info.fund_category_id = 2;
      break;
    case 'c':
      this.info.fund_category_id = 3;
      break;
    default:
      this.info.fund_category_id = 'NULL';
  }
  return this.info.fund_category_id;
};

/**
 * ������������ ��� ����� �� ������� �� � ������ ��� �����
 *
 * @returns {AISAutomation.fund}
 */
AISAutomation.fund.prototype.documentation_type_id = function (type_af) {
  switch (type_af) {
    case 1:
      this.info.documentation_type_id = 1;
      break;
    case 2:
      this.info.documentation_type_id = 4;
      break;
    case 3:
      this.info.documentation_type_id = 2;
      break;
    case 4:
      this.info.documentation_type_id = 3;
      break;
    case 5:
      this.info.documentation_type_id = 6;
      break;
    case 6:
      this.info.documentation_type_id = 7;
      break;
    case 7:
      this.info.documentation_type_id = 5;
      break;
    case 8:
      this.info.documentation_type_id = 8;
      break;
    case 9:
      this.info.documentation_type_id = 10;
      break;
    case 10:
      this.info.documentation_type_id = 9;
      break;
    default:
      this.info.documentation_type_id = 1;
  }
  return this.info.documentation_type_id;
};