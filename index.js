var AISAutomation = {};

AISAutomation.include = function(inc_file) {
  var objFs = new ActiveXObject('Scripting.FileSystemObject');
  var objLib = eval(objFs.OpenTextFile(inc_file, 1).ReadAll());
};

AISAutomation.include('include/array.js');
AISAutomation.include('include/fs.js');
AISAutomation.include('include/log.js');
AISAutomation.include('include/email.js');
AISAutomation.include('include/json2.js');
AISAutomation.include('include/xdate.js');
AISAutomation.include('include/db.js');
AISAutomation.include('include/config.js');
AISAutomation.include('include/storage.js');
AISAutomation.include('include/fund.js');
AISAutomation.include('include/inventory.js');
AISAutomation.include('include/unit.js');
AISAutomation.include('include/cli.js');

// ��� ��������� ��������� ��������� ������
AISAutomation.cli.addOptions([
  { name: '--operation', alias: '-o', type: 'arg' },
  { name: '--archive', alias: '-a', type: 'arg' },
  { name: '--clear_units', alias: '-c', type: 'opt' },
  { name: '--help', alias: '-h', type: 'opt' }
]);

// �������� � ������ ���������� ����������
AISAutomation.cli.parse();

// ���������� ��������� � ������������ � ��������� ��������� ��������� --operation
switch (AISAutomation.cli.getOptionValue('--operation')) {
  case 'sync':
    AISAutomation.include('tasks/sync.js');
    break;
  case 'fix_volumes':
    AISAutomation.include('tasks/fix-volumes.js');
    break;
  case 'merge_volumes':
    AISAutomation.include('tasks/merge-volumes.js');
    break;
  case 'photodocs_sync':
    AISAutomation.include('tasks/photodocs-sync.js');
    break;
  case 'photodocs_images':
    AISAutomation.include('tasks/photodocs-images.js');
    break;
  case 'clear_units':
    AISAutomation.include('tasks/clear-units.js');
    break;
  case 'find_inventories_duplicates':
    AISAutomation.include('tasks/find-inventories-duplicates.js');
    break;
  case 'requests':
    break;
  case 'requests_employees':
    break;
  case 'payments':
    AISAutomation.include('tasks/payments.js');
    break;
  case 'grant_free_access':
    AISAutomation.include('tasks/grant-free-access.js');
    break;
  case 'revoke_free_access':
    AISAutomation.include('tasks/revoke-free-access.js');
    break;
  default:
    throw new Error('����������� �������� ��������� --operation (-o)');
};