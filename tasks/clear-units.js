var clear_log = new AISAutomation.log(AISAutomation.config.operations.clear_units.log);
var ais_db = new AISAutomation.db(AISAutomation.config.ais.db, clear_log);
ais_db.connect();
do {
  // ������� ���������, ���������� ��� ���������
  var aisu_old_query = ais_db.query("\
    SELECT TOP 1\
      aisu.Id as aisu_id,\
      aisu.ArchiveId as aisu_archive_id,\
      aisu.Number as aisu_number,\
      aisu.Letter as aisu_letter,\
      aisu.Location as aisu_location,\
      ISNULL(aisu.Volume, 0) as aisu_volume,\
      aisi.Number as aisi_number,\
      aisi.Letter as aisi_letter,\
      ISNULL(aisi.Volume, 0) as aisi_volume,\
      aisf.RubricId as aisf_rubric_id,\
      aisf.Letter1 as aisf_letter1,\
      aisf.Number as aisf_number,\
      aisf.Letter2 as aisf_letter2\
    FROM [" + ais_db.name + "].[dbo].[tblArchiveFiles] aisu\
    INNER JOIN [" + ais_db.name + "].[dbo].[tblInventories] aisi ON\
      aisi.Id = aisu.InventoryId\
    INNER JOIN [" + ais_db.name + "].[dbo].[tblFunds] aisf ON\
      aisf.Id = aisi.FundId\
    WHERE\
      aisu.Deleted = 1\
    ORDER BY\
      aisu.Id ASC");

  if (!aisu_old_query.EOF) {
    var aisu_old = {
      id:               parseInt(aisu_old_query.Fields.Item('aisu_id')),
      archive_id:       parseInt(aisu_old_query.Fields.Item('aisu_archive_id')),
      rubric_id:        parseInt(aisu_old_query.Fields.Item('aisf_rubric_id')),
      location:         AISAutomation.db.not_null(aisu_old_query.Fields.Item('aisu_location')),
      fund_letter1:     AISAutomation.db.not_null(aisu_old_query.Fields.Item('aisf_letter1')),
      fund_number:      parseInt(aisu_old_query.Fields.Item('aisf_number')),
      fund_letter2:     AISAutomation.db.not_null(aisu_old_query.Fields.Item('aisf_letter2')),
      inventory_number: AISAutomation.db.not_null(aisu_old_query.Fields.Item('aisi_number')),
      inventory_letter: AISAutomation.db.not_null(aisu_old_query.Fields.Item('aisi_letter')),
      inventory_volume: parseInt(aisu_old_query.Fields.Item('aisi_volume')),
      unit_number:      AISAutomation.db.not_null(aisu_old_query.Fields.Item('aisu_number')),
      unit_letter:      AISAutomation.db.not_null(aisu_old_query.Fields.Item('aisu_letter')),
      unit_volume:      parseInt(aisu_old_query.Fields.Item('aisu_volume'))
    };
    clear_log.add('**********');
    clear_log.add('��������� ���������: ' + aisu_old.location + '. ArchiveFileId: ' + aisu_old.id + '. ArchiveId: ' + aisu_old.archive_id + '. RubricId: ' + aisu_old.rubric_id + '.');

    clear_log.add('�������� ������� ' + aisu_old.location);
    var aisu_delete_query = ais_db.query("\
      DELETE FROM [" + ais_db.name + "].[dbo].[tblStorage]\
      WHERE\
        ItemType = 5\
        AND ItemId = " + aisu_old.id);

    clear_log.add('�������� ���������� �� ������� tblItemsAdditionalFieldsValues');
    var aisu_delete_query = ais_db.query("\
      DELETE FROM [" + ais_db.name + "].[dbo].[tblItemsAdditionalFieldsValues]\
      WHERE\
        ItemType = 5\
        AND ItemId = " + aisu_old.id);

    clear_log.add('�������� ���������� �� ������� tblArchiveFilesToDictionaryCustom');
    var aisu_delete_query = ais_db.query("\
      DELETE FROM [" + ais_db.name + "].[dbo].[tblArchiveFilesToDictionaryCustom]\
      WHERE\
        ArchiveFileId = " + aisu_old.id);

    clear_log.add('�������� ���������� �� ������� tblArchiveFilesToEkdi');
    var aisu_delete_query = ais_db.query("\
      DELETE FROM [" + ais_db.name + "].[dbo].[tblArchiveFilesToEkdi]\
      WHERE\
        ArchiveFileId = " + aisu_old.id);

    clear_log.add('�������� ���������� �� ������� tblArchiveFilesToEkdiAdditional');
    var aisu_delete_query = ais_db.query("\
      DELETE FROM [" + ais_db.name + "].[dbo].[tblArchiveFilesToEkdiAdditional]\
      WHERE\
        ArchiveFileId = " + aisu_old.id);

    clear_log.add('�������� ���������� �� ������� tblArchiveFilesTopology');
    var aisu_delete_query = ais_db.query("\
      DELETE FROM [" + ais_db.name + "].[dbo].[tblArchiveFilesTopology]\
      WHERE\
        ArchiveFileId = " + aisu_old.id);

    clear_log.add('�������� ���������� �� ������� tblDocuments');
    var aisu_delete_query = ais_db.query("\
      DELETE FROM [" + ais_db.name + "].[dbo].[tblDocuments]\
      WHERE\
        ArchiveFileId = " + aisu_old.id);

    // ����� � �� ��������������� ����������� ��������� � ����� �� Location, ArchiveId, RubricId
    var aisu_new_query = ais_db.query("\
      SELECT TOP 1\
        aisu.Id\
      FROM [" + ais_db.name + "].[dbo].[tblArchiveFiles] aisu\
      INNER JOIN [" + ais_db.name + "].[dbo].[tblInventories] aisi ON\
        aisi.Id = aisu.InventoryId\
      INNER JOIN [" + ais_db.name + "].[dbo].[tblFunds] aisf ON\
        aisf.Id = aisi.FundId\
      WHERE\
        aisu.Deleted = 0\
        AND aisu.ArchiveId = " + aisu_old.archive_id + "\
        AND aisf.RubricId = " + aisu_old.rubric_id + "\
        AND aisu.Number LIKE " + AISAutomation.db.wrap(aisu_old.unit_number) + "\
        AND aisu.Letter LIKE " + AISAutomation.db.wrap(aisu_old.unit_letter) + "\
        AND ISNULL(aisu.Volume, 0) = " + aisu_old.unit_volume + "\
        AND aisi.Number LIKE " + AISAutomation.db.wrap(aisu_old.inventory_number) + "\
        AND aisi.Letter LIKE " + AISAutomation.db.wrap(aisu_old.inventory_letter) + "\
        AND ISNULL(aisi.Volume, 0) = " + aisu_old.inventory_volume + "\
        AND aisf.Letter1 LIKE " + AISAutomation.db.wrap(aisu_old.fund_letter1) + "\
        AND aisf.Number = " + aisu_old.fund_number + "\
        AND aisf.Letter2 LIKE " + AISAutomation.db.wrap(aisu_old.fund_letter2) + "\
      ORDER BY\
        aisu.Id DESC");

    if (aisu_new_query.EOF) {
      clear_log.add('��� ���������� ��������� �� ���������� ���������������� ������������ ���������');
      // �������� ������ � ������� Paid.OrderItem
      clear_log.add('�������� ������ � ������� Paid.OrderItem');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[Paid.OrderItem] WHERE ItemTypeId = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblRemarks
      clear_log.add('�������� ������ � ������� tblRemarks');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblRemarks] WHERE Id IN (\
        SELECT\
          aisr.Id\
        FROM [" + ais_db.name + "].[dbo].[tblRemarks] aisr\
        INNER JOIN [" + ais_db.name + "].[dbo].[tblBookmarks] aisb ON\
          aisb.Id = aisr.BookmarkId\
        WHERE\
          aisb.ItemType = 5\
          AND aisb.ItemId = " + aisu_old.id + ")");
      // �������� ������ � ������� tblBookmarks
      clear_log.add('�������� ������ � ������� tblBookmarks');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblBookmarks] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblEntityViewStatistics
      clear_log.add('�������� ������ � ������� tblEntityViewStatistics');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblEntityViewStatistics] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblItemsHistory
      clear_log.add('�������� ������ � ������� tblItemsHistory');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblItemsHistory] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblItemViewStatistics
      clear_log.add('�������� ������ � ������� tblItemViewStatistics');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblItemViewStatistics] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblPlanWorkFiles
      clear_log.add('�������� ������ � ������� tblPlanWorkFiles');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblPlanWorkFiles] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblQueue
      clear_log.add('�������� ������ � ������� tblQueue');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblQueue] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblRecommendations
      clear_log.add('�������� ������ � ������� tblRecommendations');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblRecommendations] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblRequestFilesLog
      clear_log.add('�������� ������ � ������� tblRequestFilesLog');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblRequestFilesLog] WHERE Id IN (\
        SELECT\
          aisrfl.Id\
        FROM [" + ais_db.name + "].[dbo].[tblRequestFilesLog] aisrfl\
        INNER JOIN [" + ais_db.name + "].[dbo].[tblRequestFiles] aisrf ON\
          aisrfl.RequestFileId = aisrf.Id\
        WHERE\
          aisrf.ItemType = 5\
          AND aisrf.ItemId = " + aisu_old.id + ")");
      // �������� ������ � ������� tblRequestFiles
      clear_log.add('�������� ������ � ������� tblRequestFiles');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblRequestFiles] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblRequestTempFiles
      clear_log.add('�������� ������ � ������� tblRequestTempFiles');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblRequestTempFiles] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblSubscriptionEvents
      clear_log.add('�������� ������ � ������� tblSubscriptionEvents');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblSubscriptionEvents] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // �������� ������ � ������� tblSubscriptions
      clear_log.add('�������� ������ � ������� tblSubscriptions');
      var aisu_paid_order_item_update_query = ais_db.query("DELETE FROM [" + ais_db.name + "].[dbo].[tblSubscriptions] WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
    } else {
      var aisu_new = {
        id: parseInt(aisu_new_query.Fields.Item('Id'))
      };
      clear_log.add('����� ������������� ������ ArchiveFileId ' + aisu_old.id + ' �� ' + aisu_new.id);

      // ���������� ������ � ������� Paid.OrderItem
      clear_log.add('���������� ������ � ������� Paid.OrderItem');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[Paid.OrderItem] SET ItemId = " + aisu_new.id + " WHERE ItemTypeId = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblBookmarks
      clear_log.add('���������� ������ � ������� tblBookmarks');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblBookmarks] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblEntityViewStatistics
      clear_log.add('���������� ������ � ������� tblEntityViewStatistics');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblEntityViewStatistics] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblItemsHistory
      clear_log.add('���������� ������ � ������� tblItemsHistory');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblItemsHistory] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblItemViewStatistics
      clear_log.add('���������� ������ � ������� tblItemViewStatistics');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblItemViewStatistics] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblPlanWorkFiles
      clear_log.add('���������� ������ � ������� tblPlanWorkFiles');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblPlanWorkFiles] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblQueue
      clear_log.add('���������� ������ � ������� tblQueue');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblQueue] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblRecommendations
      clear_log.add('���������� ������ � ������� tblRecommendations');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblRecommendations] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblRequestFiles
      clear_log.add('���������� ������ � ������� tblRequestFiles');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblRequestFiles] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblRequestTempFiles
      clear_log.add('���������� ������ � ������� tblRequestTempFiles');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblRequestTempFiles] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblSubscriptionEvents
      clear_log.add('���������� ������ � ������� tblSubscriptionEvents');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblSubscriptionEvents] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
      // ���������� ������ � ������� tblSubscriptions
      clear_log.add('���������� ������ � ������� tblSubscriptions');
      var aisu_paid_order_item_update_query = ais_db.query("UPDATE [" + ais_db.name + "].[dbo].[tblSubscriptions] SET ItemId = " + aisu_new.id + " WHERE ItemType = 5 AND ItemId = " + aisu_old.id);
    }

    clear_log.add('�������� ���������� � ������� ��������');
    var aisu_delete_query = ais_db.query("\
      DELETE FROM [" + ais_db.name + "].[dbo].[tblArchiveFiles]\
      WHERE\
        Id = " + aisu_old.id);
  }
} while (!aisu_old_query.EOF)
ais_db.disconnect();
clear_log.close();