var sync_config = AISAutomation.config.operations.photodocs_images.archives[AISAutomation.cli.getOptionValue('--archive')];
var sync_log = new AISAutomation.log(sync_config.log);
var ais_db = new AISAutomation.db(AISAutomation.config.ais.db, sync_log);
ais_db.connect();

sync_log.add('����� ������� ��������������', 'info');
var current_doc_seq = 0;
do {
  current_doc_seq++;
  var photodocs = ais_db.query("\
    ;WITH Photodocs AS (\
      SELECT\
        aispd.Id,\
        ISNULL(aispi.Number, 0) as InventoryNumber,\
        ISNULL(aispi.Letter, '') as InventoryLetter,\
        ISNULL(aispd.Number, 0) as PhotodocNumber,\
        ISNULL(aispd.Letter, '') as PhotodocLetter,\
        ROW_NUMBER() OVER(ORDER BY aispd.Id) AS seq,\
        ROW_NUMBER() OVER(ORDER BY aispd.Id DESC) AS totrows\
      FROM [" + ais_db.name + "].[dbo].[tblPhotoDocuments] aispd\
      INNER JOIN [" + ais_db.name + "].[dbo].[tblPhotoInventories] aispi ON\
        aispi.Id = aispd.PhotoInventoryId\
      WHERE\
        aispd.Deleted = 0\
        AND aispd.ArchiveId = " + sync_config.archive_id + "\
      )\
      SELECT\
        *\
      FROM Photodocs\
      WHERE seq =" + current_doc_seq);
  if (!photodocs.EOF) {
    sync_log.add('������������ ��������� ����������� �������. ����� ������� �������������', 'debug');
    var inventory_number = AISAutomation.db.not_null(photodocs.Fields.Item('InventoryNumber'));
    var inventory_letter = AISAutomation.db.not_null(photodocs.Fields.Item('InventoryLetter'));
    var inventory_full_number = inventory_number + inventory_letter;
    var photodoc_id = AISAutomation.db.not_null(photodocs.Fields.Item('Id'));
    var photodoc_number = AISAutomation.db.not_null(photodocs.Fields.Item('PhotodocNumber'));
    var photodoc_letter = AISAutomation.db.not_null(photodocs.Fields.Item('PhotodocLetter'));
    var photodoc_full_number = photodoc_number + photodoc_letter;
    for (var storageID = 0; storageID < sync_config.storage.units_root.length; storageID++) {
      // ������ ��� �������� ���������� �� �������,
      // ������� ������ ��� ���� ������� �����,
      // ������� ��� �������
      sync_log.add("�������� ������ �� ������ ������������� " + photodoc_full_number + " ����� " + inventory_full_number, "info");
      var clear_unit_images = ais_db.query("DELETE FROM [tblStorage] WHERE ItemType = 45 AND ItemId = " + photodoc_id);
      sync_log.add("����� ����������� ������� ������������� " + photodoc_full_number + " ����� " + photodoc_full_number + " � ��������� " + sync_config.storage.units_root[storageID], "info");

      var unit_storage = new AISAutomation.storage({
        log:              sync_log,
        root:             sync_config.storage.units_root[storageID],
        extensions:       sync_config.storage.extensions,
        fund_prefix:      '',
        fund_number:      '',
        fund_letter:      '',
        inventory_number: inventory_number,
        inventory_letter: inventory_letter,
        inventory_volume: '',
        unit_number:      photodoc_number,
        unit_letter:      photodoc_letter
      });

      // �������� ������ ������
      var unit_images = unit_storage.get_files();
      // ���� ����� � ����� ����, �������, ��� ����� ���������
      // ��������������� �� ����� � �������� ������ �� ������
      if (unit_images.length > 0) {
        // ���� �� ��������� ��� ������ ������� ��������,
        // ����� ��������� ������ �������.
        for (var image = 0; image < unit_images.length; image++) {
          sync_log.add("���������� ���������� � ����� " + unit_images[image]['path'] + " � ������������� " + photodoc_full_number + " ����� " + inventory_full_number, "debug");
          // ������� ���������� � ����� ������ � �� ���. ���� ��� ��� ��� ����, ��������
          var aiss = ais_db.query("INSERT INTO [dbo].[tblStorage]\
            (ItemId, ItemType, StorageType, Name, Path, TotalPages, Deleted, CreationTime, Hidden)\
            VALUES (" + photodoc_id + ", 45, 1, " + AISAutomation.db.wrap(AISAutomation.db.safe(unit_images[image]['name'])) + ", " + AISAutomation.db.wrap(AISAutomation.db.safe(unit_images[image]['path'])) + ", 1, 0, CURRENT_TIMESTAMP, 0)");
        }
        break;
      }
    }
  }
} while (!photodocs.EOF)

ais_db.disconnect();
sync_log.close();