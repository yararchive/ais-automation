var sync_config = AISAutomation.config.operations.sync.archives[AISAutomation.cli.getOptionValue('--archive')];
var sync_log = new AISAutomation.log(AISAutomation.config.operations.fix_volumes.archives[AISAutomation.cli.getOptionValue('--archive')].log);
var sync_db = new AISAutomation.db(sync_config.db, sync_log);
sync_db.connect();
var ais_db = new AISAutomation.db(AISAutomation.config.ais.db, sync_log);
ais_db.connect();

sync_log.add('������� ������ � ������ �� ��������� �����', 'info');
var current_inv_volume_seq = 0;
do {
  current_inv_volume_seq++;
  var inv_volumes = sync_db.query("\
    ;WITH InventoriesWithVolumes AS (\
      SELECT\
        aff.ISN_FUND,\
        afi.ISN_INVENTORY,\
        ROW_NUMBER() OVER(ORDER BY afi.ISN_INVENTORY) AS seq,\
        ROW_NUMBER() OVER(ORDER BY afi.ISN_INVENTORY DESC) AS totrows\
      FROM [" + sync_db.name + "].[dbo].[tblINVENTORY] afi\
      INNER JOIN [" + sync_db.name + "].[dbo].[tblFUND] aff ON\
        aff.ISN_FUND = afi.ISN_FUND\
      WHERE\
        ISNULL(afi.INVENTORY_NUM_3, 0) <> 0\
        AND ISNULL(afi.ISN_SECURLEVEL, 0) <> 2\
        AND ISNULL(afi.PRESENCE_FLAG, '') NOT LIKE 'b'\
        AND afi.Deleted = 0\
      )\
      SELECT\
        ISN_FUND,\
        ISN_INVENTORY\
      FROM InventoriesWithVolumes\
      WHERE seq = " + current_inv_volume_seq);
  if (!inv_volumes.EOF) {
    var fund = new AISAutomation.fund({
      'ais_db': ais_db,
      'sync_db': sync_db,
      'log': sync_log,
      'restrictions': sync_config.restrictions,
      'archive_id': sync_config.archive_id,
      'rubric_id': sync_config.rubric_id,
      'ignore_inventories_volumes': false,
      'storage': sync_config.storage,
      'id': parseInt(AISAutomation.db.not_null(inv_volumes.Fields.Item('ISN_FUND'))),
      'allowed_isn_security_reason': sync_config.allowed_isn_security_reason
    });

    fund.load_from_af();
    fund.find_id_in_ais();
    fund.apply_resctrictions();
    
    var inventory = new AISAutomation.inventory({
      'fund': fund,
      'id': parseInt(AISAutomation.db.not_null(inv_volumes.Fields.Item('ISN_INVENTORY')))
    });

    inventory.load_from_af();
    inventory.af_to_ais();
    inventory.apply_resctrictions();
    inventory.scan_storage();

    var inv_min_unit = sync_db.query('\
      SELECT\
        ISNULL(MIN(CONVERT(int, UNIT_NUM_1)), 0) AS minimun\
      FROM [' + sync_db.name + '].[dbo].[tblUNIT]\
      WHERE\
        ISN_INVENTORY = ' + inventory.id);
    inv_min_unit_num =  parseInt(AISAutomation.db.not_null(inv_min_unit.Fields.Item('minimun')));

    var inv_max_unit = sync_db.query('\
      SELECT\
        ISNULL(MAX(CONVERT(int, UNIT_NUM_1)), 0) AS minimun\
      FROM [' + sync_db.name + '].[dbo].[tblUNIT]\
      WHERE\
        ISN_INVENTORY = ' + inventory.id);
    inv_max_unit_num =  parseInt(AISAutomation.db.not_null(inv_max_unit.Fields.Item('minimun')));

    sync_log.add('� ����� ' + inventory.full_number + ' ����� ' + fund.full_number + ' ����������� ����� ���� = ' + inv_min_unit_num + ', ������������ = ' + inv_max_unit_num, 'info');
    sync_log.add('������� ��������� ��� � ����� �����');
    ais_db.query("\
      UPDATE [" + ais_db.name + "].[dbo].[tblArchiveFiles]\
      SET\
        InventoryId = " + inventory.info.id + "\
      WHERE\
        Id IN (\
          SELECT\
            aisu.Id\
          FROM [" + ais_db.name + "].[dbo].[tblArchiveFiles] aisu\
          INNER JOIN [" + ais_db.name + "].[dbo].[tblInventories] aisi ON\
            aisi.Id = aisu.InventoryId\
          INNER JOIN [" + ais_db.name + "].[dbo].[tblFunds] aisf ON\
            aisf.Id = aisi.FundId\
          WHERE\
            aisu.ArchiveId = " + fund.info.archive_id + "\
            AND aisf.RubricId = " + fund.info.rubric_id + "\
            AND aisf.Letter1 LIKE " + AISAutomation.db.wrap(fund.info.letter1) + "\
            AND aisf.Number = " + fund.info.number + "\
            AND aisf.Letter2 LIKE " + AISAutomation.db.wrap(fund.info.letter2) + "\
            AND ISNULL(aisi.Number, 0) = " + inventory.info.number + "\
            AND ISNULL(aisi.Letter, '') LIKE " + AISAutomation.db.wrap(inventory.info.letter) + "\
            AND ISNULL(aisi.Volume, 0) = 0\
            AND ISNULL(aisu.Number, 0) >= " + inv_min_unit_num + "\
            AND ISNULL(aisu.Number, 0) <= " + inv_max_unit_num + "\
        )");

    inventory.recalc_units();
    fund.clear_inventories();
    fund.recalc_inventories();
    fund.recalc_units();
  }
} while (!inv_volumes.EOF)
sync_db.disconnect();
ais_db.disconnect();
sync_log.close();