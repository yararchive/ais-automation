var sync_config = AISAutomation.config.operations.photodocs_sync.archives[AISAutomation.cli.getOptionValue('--archive')];
var sync_log = new AISAutomation.log(sync_config.log);
var sync_db = new AISAutomation.db(sync_config.db, sync_log);
sync_db.connect();
var ais_db = new AISAutomation.db(AISAutomation.config.ais.db, sync_log);
ais_db.connect();

sync_log.add('������� �������������� �� �� ' + sync_db.name, 'info');
var current_doc_seq = 0;
do {
  current_doc_seq++;
  var photodocs = sync_db.query("\
    ;WITH Photodocs AS (\
      SELECT\
        pd.*,\
        ROW_NUMBER() OVER(ORDER BY pd.UnitNumber) AS seq,\
        ROW_NUMBER() OVER(ORDER BY pd.UnitNumber DESC) AS totrows\
      FROM [" + sync_db.name + "].[dbo].[cards] pd\
      )\
      SELECT\
        *\
      FROM Photodocs\
      WHERE seq = " + current_doc_seq);
  if (!photodocs.EOF) {
    var doc_values = {
      Deleted: 0,
      CreationTime: 'CURRENT_TIMESTAMP',
      ArchiveId: sync_config.archive_id,
      PhotoInventoryId: AISAutomation.db.not_null(photodocs.Fields.Item('InventoryId'), { replace_str: 0, wrap: true, safe: true }),
      Letter: AISAutomation.db.not_null(photodocs.Fields.Item('UnitLetter'), { wrap: true, safe: true }),
      '[Index]': AISAutomation.db.not_null(photodocs.Fields.Item('UnitIndex'), { wrap: true, safe: true }),
      Annotation: AISAutomation.db.not_null(photodocs.Fields.Item('UnitName'), { wrap: true, safe: true }),
      Author: AISAutomation.db.not_null(photodocs.Fields.Item('UnitAuthor'), { wrap: true, safe: true }),
      Description: AISAutomation.db.not_null(photodocs.Fields.Item('UnitNote'), { wrap: true, safe: true }),
      RecordPlace: AISAutomation.db.not_null(photodocs.Fields.Item('UnitPlace'), { wrap: true, safe: true }),
      IsExactRecordDate: 0,
      RecordDate: AISAutomation.db.not_null(photodocs.Fields.Item('UnitDate'), { wrap: true, safe: true }),
      Number: AISAutomation.db.not_null(photodocs.Fields.Item('UnitNumber'), { replace_str: 0, wrap: true, safe: true }),
      Published: 0,
      Quantity: AISAutomation.db.not_null(photodocs.Fields.Item('UnitPagesCount'), { replace_str: 0, wrap: true, safe: true }),
      IsHidden: 0
    };
    var doc = ais_db.insertOrId({
      table: '[dbo].[tblPhotoDocuments]',
      condition: '[Number] = ' + doc_values.Number +
        ' AND ISNULL([Letter], \'\') LIKE ' + doc_values.Letter +
        ' AND [PhotoInventoryId] = ' + doc_values.PhotoInventoryId +
        ' AND [ArchiveId] = ' + doc_values.ArchiveId,
      id_column: "Id",
      values: doc_values,
      need_to_be_updated: true
    });
  }
} while (!photodocs.EOF)

sync_db.disconnect();
ais_db.disconnect();
sync_log.close();