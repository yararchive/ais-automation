var search_log = new AISAutomation.log(AISAutomation.config.operations.find_inventories_duplicates.log);
var ais_db = new AISAutomation.db(AISAutomation.config.ais.db, search_log);
ais_db.connect();
// ������� ���������� � ������������� ������
var aisi_dup_query = ais_db.query("\
  SELECT\
    aisa.Id AS ArchiveId,\
    aisa.ShortName AS ArchiveName,\
    aisf.RubricId AS RubricId,\
    aisr.Name AS RubricName,\
    aisf.Letter1 AS FundLetter1,\
    aisf.Number AS FundNumber,\
    aisf.Letter2 AS FundLetter2,\
    aisi.Number AS InventoryNumber,\
    aisi.Letter AS InventoryLetter,\
    aisi.Volume AS InventoryVolume,\
    COUNT(*) AS DuplicatesCount\
  FROM [" + ais_db.name + "].[dbo].[tblInventories] aisi\
  INNER JOIN [" + ais_db.name + "].[dbo].[tblFunds] aisf ON\
    aisf.Id = aisi.FundId\
  INNER JOIN [" + ais_db.name + "].[dbo].[tblArchives] aisa ON\
    aisa.Id = aisf.ArchiveId\
  INNER JOIN [" + ais_db.name + "].[dbo].[tblRubrics] aisr ON\
    aisr.Id = aisf.RubricId\
  WHERE\
    aisi.Deleted = 0\
    AND aisf.Deleted = 0\
    AND aisa.Deleted = 0\
  GROUP BY\
    aisa.Id,\
    aisa.ShortName,\
    aisf.RubricId,\
    aisr.Name,\
    aisf.Letter1,\
    aisf.Number,\
    aisf.Letter2,\
    aisi.Number,\
    aisi.Letter,\
    aisi.Volume\
  HAVING (COUNT(*) > 1)");

if (!aisi_dup_query.EOF) {
  while (!aisi_dup_query.EOF) {
    var aisi_dup = {
      archive_id: parseInt(aisi_dup_query.Fields.Item('ArchiveId')),
      archive_name: AISAutomation.db.not_null(aisi_dup_query.Fields.Item('ArchiveName')),
      rubric_id: parseInt(aisi_dup_query.Fields.Item('RubricId')),
      rubric_name: AISAutomation.db.not_null(aisi_dup_query.Fields.Item('RubricName')),
      fund_letter_1: AISAutomation.db.not_null(aisi_dup_query.Fields.Item('FundLetter1')),
      fund_number: parseInt(aisi_dup_query.Fields.Item('FundNumber')),
      fund_letter_2: AISAutomation.db.not_null(aisi_dup_query.Fields.Item('FundLetter2')),
      inventory_number: AISAutomation.db.not_null(aisi_dup_query.Fields.Item('InventoryNumber')),
      inventory_letter: AISAutomation.db.not_null(aisi_dup_query.Fields.Item('InventoryLetter')),
      inventory_volume: AISAutomation.db.not_null(aisi_dup_query.Fields.Item('InventoryVolume')),
      duplicates_count: parseInt(aisi_dup_query.Fields.Item('DuplicatesCount'))
    }

    search_log.add('ArchiveId: ' + aisi_dup.archive_id + '.  ArchiveName: ' + aisi_dup.archive_name + '. RubricId: ' + aisi_dup.rubric_id + '. RubricName: ' + aisi_dup.rubric_name + '. FundLetter1: ' + aisi_dup.fund_letter_1 + '. FundNumber: ' + aisi_dup.fund_number + '. FundLetter2: ' + aisi_dup.fund_letter_2 + '. InventoryNumber: ' + aisi_dup.inventory_number + '. InventoryLetter: ' + aisi_dup.inventory_letter + '. InventoryVolume: ' + aisi_dup.inventory_volume + '. DuplicatesCount: ' + aisi_dup.duplicates_count + '.');

    aisi_dup_query.moveNext();
  }
}
ais_db.disconnect();
search_log.close();