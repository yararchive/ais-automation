var free_access_config = AISAutomation.config.operations.grant_free_access;
var access_log = new AISAutomation.log(free_access_config.log);
var ais_db = new AISAutomation.db(AISAutomation.config.ais.db, access_log);
ais_db.connect();

var current_user_seq = 0;
do {
  current_user_seq++;

  var aisu = ais_db.query("\
    ;WITH UsersList AS (\
      SELECT\
        UserId,\
        ROW_NUMBER() OVER(ORDER BY UserId) AS seq,\
        ROW_NUMBER() OVER(ORDER BY UserId DESC) AS totrows\
      FROM [" + ais_db.name + "].[dbo].[aspnet_Users]\
      WHERE\
        UserName LIKE '" + free_access_config.login_mask + "'\
    )\
    SELECT\
      UserId\
    FROM UsersList\
    WHERE seq = " + current_user_seq);

  if (!aisu.EOF) {
    var ais_user_query = ais_db.query("SELECT TOP 1\
      aisusers.UserName,\
      aism.Email,\
      aisup.LastName,\
      aisup.FirstName,\
      aisup.MiddleName\
    FROM [" + ais_db.name + "].[dbo].[aspnet_Users] aisusers\
    INNER JOIN [" + ais_db.name + "].[dbo].[aspnet_Membership] aism ON\
      aism.UserId = aisusers.UserId\
    INNER JOIN [AISArchive].[dbo].[tblUserProfiles] aisup ON\
      aisup.UserId = aisusers.UserId\
    WHERE\
      aisusers.UserId = '" + aisu.Fields.Item('UserId') + "'\
    ");
    access_log.add("��������� ���� ������� � ������������ ���������� � ������������ " +
      ais_user_query.Fields.Item('LastName') + " " +
      ais_user_query.Fields.Item('FirstName') + " " +
      ais_user_query.Fields.Item('MiddleName') + " (" +
      ais_user_query.Fields.Item('UserName') + "), ID " +
      aisu.Fields.Item('UserId'), "info");

    // ����� �� �������������� ����� � ��� �������������, � ������� ��� ����,
    // ���� ������� ����� �� ������ ����������
    var delete_role_query = ais_db.query("\
      DELETE\
        FROM [" + ais_db.name + "].[dbo].[aspnet_UsersInRoles]\
      WHERE\
        UserId = '" + aisu.Fields.Item('UserId') + "'\
        AND RoleId = '" + AISAutomation.config.operations.payments.role_id + "'\
    ");

    // ���������� ����� �� ������ ����������
    var add_role_query = ais_db.query("\
      INSERT\
        INTO [" + ais_db.name + "].[dbo].[aspnet_UsersInRoles]\
        (UserId, RoleId)\
      VALUES (\
        '" + aisu.Fields.Item('UserId') + "',\
        '" + AISAutomation.config.operations.payments.role_id + "'\
      )\
    ");
  }
} while (!aisu.EOF)

access_log.add("���������� �������������: " + (current_user_seq - 1), "info");
ais_db.disconnect();
access_log.close();