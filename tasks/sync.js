var sync_config = AISAutomation.config.operations.sync.archives[AISAutomation.cli.getOptionValue('--archive')];
var sync_log = new AISAutomation.log(sync_config.log);
var sync_db = new AISAutomation.db(sync_config.db, sync_log);
sync_db.connect();
var ais_db = new AISAutomation.db(AISAutomation.config.ais.db, sync_log);
ais_db.connect();

// ��� ������ ���������� ���������, ��������� �� ������� � Id �� ������������ � Id ������ ������ ��
sync_log.add('�������� ������������� ��������� � ������������ ������� ��� ������� ������', 'debug');
var check_ais_rubric = parseInt(ais_db.query('\
  SELECT COUNT(*) AS result\
  FROM [' + AISAutomation.config.ais.db.name + '].[dbo].[tblRubrics]\
  WHERE Id = ' + sync_config.rubric_id + ' AND ArchiveId = ' + sync_config.archive_id + ' AND Deleted = 0').Fields.Item('result'));
if (check_ais_rubric) {
  sync_log.add('������� ����������', 'debug');
} else {
  sync_log.add('��������� � ������������ ������� �� ����������', 'error');
}

sync_log.add('������ ��������� ������ ������ � ID ' + sync_config.archive_id, 'info');
var current_fund_seq = 0;
do {
  current_fund_seq++;

  var aff = sync_db.query("\
    ;WITH FundsNumbers AS (\
      SELECT\
        ISN_FUND,\
        ROW_NUMBER() OVER(ORDER BY ISN_FUND) AS seq,\
        ROW_NUMBER() OVER(ORDER BY ISN_FUND DESC) AS totrows\
      FROM [" + sync_config.db.name + "].[dbo].[tblFUND]\
      WHERE\
        Deleted = 0\
        AND ISNULL(ISN_SECURLEVEL, 0) <> 2\
        AND ISNULL(PRESENCE_FLAG, '') NOT LIKE 'b'\
        AND ISNULL(FUND_NUM_1, '') LIKE ''\
    )\
    SELECT\
      ISN_FUND\
    FROM FundsNumbers\
    WHERE seq = " + current_fund_seq);

  if (!aff.EOF) {
    var fund = new AISAutomation.fund({
      'ais_db': ais_db,
      'sync_db': sync_db,
      'log': sync_log,
      'restrictions': sync_config.restrictions,
      'archive_id': sync_config.archive_id,
      'rubric_id': sync_config.rubric_id,
      'ignore_inventories_volumes': sync_config.ignore_inventories_volumes,
      'storage': sync_config.storage,
      'allowed_isn_security_reason': sync_config.allowed_isn_security_reason,
      'id': parseInt(AISAutomation.db.not_null(aff.Fields.Item('ISN_FUND')))
    });

    fund.load_from_af();
    fund.af_to_ais();
    fund.apply_resctrictions();
    fund.process_inventories();
    fund.clear_inventories();
    fund.recalc_inventories();
    fund.recalc_units();
  }
} while (!aff.EOF)
sync_log.add('��������� ��������� ������ ������ � ID ' + sync_config.archive_id, 'info');
sync_log.add('�������� ������ ������ ������ � ID ' + sync_config.archive_id, 'info');
ais_db.query("\
  UPDATE [" + ais_db.name + "].[dbo].[tblFunds]\
    SET\
      Deleted = 1\
    WHERE\
      Id IN (\
        SELECT aisf.Id\
          FROM [" + ais_db.name + "].[dbo].[tblFunds] aisf\
          WHERE\
            aisf.ArchiveId = " + sync_config.archive_id + "\
            AND aisf.RubricId = " + sync_config.rubric_id + "\
            AND aisf.Deleted = 0\
            AND NOT EXISTS (\
              SELECT aff.ID\
              FROM [" + sync_db.name + "].[dbo].[tblFUND] aff\
              WHERE\
                ISNULL(aff.FUND_NUM_1, '') LIKE ISNULL(aisf.Letter1, '')\
                AND ISNULL(aff.FUND_NUM_2, 0) = ISNULL(aisf.Number, 0)\
                AND ISNULL(aff.FUND_NUM_3, '') LIKE ISNULL(aisf.Letter2, '')\
                AND aff.Deleted = 0\
                AND ISNULL(aff.PRESENCE_FLAG, '') NOT LIKE 'b'\
                AND ISNULL(aff.ISN_SECURLEVEL, 0) <> 2\
            )\
      )");
sync_db.disconnect();
ais_db.disconnect();
sync_log.close();