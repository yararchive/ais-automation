var sync_config = AISAutomation.config.operations.sync.archives[AISAutomation.cli.getOptionValue('--archive')];
var sync_log = new AISAutomation.log(AISAutomation.config.operations.merge_volumes.archives[AISAutomation.cli.getOptionValue('--archive')].log);
var sync_db = new AISAutomation.db(sync_config.db, sync_log);
sync_db.connect();
var ais_db = new AISAutomation.db(AISAutomation.config.ais.db, sync_log);
ais_db.connect();

sync_log.add('������� ������ � ������ �� ��������� �����', 'info');
var current_inv_volume_seq = 0;
do {
  current_inv_volume_seq++;
  var inv_volumes = ais_db.query("\
      SELECT TOP 1\
        aisi.Id,\
        aisi.FundId\
      FROM [" + ais_db.name + "].[dbo].[tblInventories] aisi\
      INNER JOIN [" + ais_db.name + "].[dbo].[tblFunds] aisf ON\
        aisf.Id = aisi.FundId\
      WHERE\
        ISNULL(aisi.Volume, 0) <> 0\
        AND aisi.Deleted = 0\
        AND aisf.ArchiveId = " + sync_config.archive_id + "\
        AND aisf.RubricId = " + sync_config.rubric_id);
  if (!inv_volumes.EOF) {
    var fund = new AISAutomation.fund({
      'ais_db': ais_db,
      'sync_db': sync_db,
      'log': sync_log,
      'restrictions': sync_config.restrictions,
      'archive_id': sync_config.archive_id,
      'rubric_id': sync_config.rubric_id,
      'ignore_inventories_volumes': true,
      'storage': sync_config.storage,
      'id': parseInt(AISAutomation.db.not_null(inv_volumes.Fields.Item('FundId')))
    });

    fund.load_from_ais();
    fund.find_id_in_ais();
    fund.apply_resctrictions();

    var inventory = new AISAutomation.inventory({
      'fund': fund,
      'id': parseInt(AISAutomation.db.not_null(inv_volumes.Fields.Item('Id')))
    });

    inventory.load_from_ais();
    inventory.af_to_ais();
    inventory.apply_resctrictions();
    inventory.scan_storage();

    sync_log.add('����������� ����� ����� ' + inventory.full_number + ' ����� ' + fund.full_number, 'info');
    ais_db.query("\
      UPDATE [" + ais_db.name + "].[dbo].[tblArchiveFiles]\
      SET\
        InventoryId = " + inventory.info.id + "\
      WHERE\
        InventoryId IN (\
          SELECT\
            aisi.Id\
          FROM [" + ais_db.name + "].[dbo].[tblInventories] aisi\
          WHERE\
            aisi.FundId = " + fund.info.id + "\
            AND ISNULL(aisi.Number, 0) = " + inventory.info.number + "\
            AND ISNULL(aisi.Letter, '') LIKE " + AISAutomation.db.wrap(inventory.info.letter) + "\
            AND ISNULL(aisi.Volume, 0) <> 0\
        )");

    sync_log.add('�������� ����� ����� ' + inventory.full_number + ' ����� ' + fund.full_number, 'info');
    ais_db.query("\
      UPDATE [" + ais_db.name + "].[dbo].[tblInventories]\
      SET\
        Deleted = 1\
      WHERE\
        Id IN (\
          SELECT\
            aisi.Id\
          FROM [" + ais_db.name + "].[dbo].[tblInventories] aisi\
          WHERE\
            aisi.FundId = " + fund.info.id + "\
            AND ISNULL(aisi.Number, 0) = " + inventory.info.number + "\
            AND ISNULL(aisi.Letter, '') LIKE " + AISAutomation.db.wrap(inventory.info.letter) + "\
            AND ISNULL(aisi.Volume, 0) <> 0\
        )");

    inventory.recalc_units();
    fund.recalc_inventories();
    fund.recalc_units();
  }
} while (!inv_volumes.EOF)
sync_db.disconnect();
ais_db.disconnect();
sync_log.close();