var free_access_config = AISAutomation.config.operations.revoke_free_access;
var payments_config = AISAutomation.config.operations.payments;
var access_log = new AISAutomation.log(free_access_config.log);
var ais_db = new AISAutomation.db(AISAutomation.config.ais.db, access_log);
ais_db.connect();

// �������� ������ �������������, � ������� ������ �������
var payments = AISAutomation.fs.textFileToString(payments_config.payments_file);
var logins = [];
var logins_query_condition = "";
try {
  payments = JSON.parse(payments);
  for (var i = 0; i < payments.length; i++) {
    logins.push(payments[i].login);
  }
  if (logins.length > 0) {
    logins_query_condition = "AND UserName NOT IN ('" + logins.join("','") + "')";
  }
} catch(e) {
  access_log.add(e.name + ". " + e.message, "warn")
}

var current_user_seq = 0;
do {
  current_user_seq++;

  var aisu = ais_db.query("\
    ;WITH UsersList AS (\
      SELECT\
        UserId,\
        ROW_NUMBER() OVER(ORDER BY UserId) AS seq,\
        ROW_NUMBER() OVER(ORDER BY UserId DESC) AS totrows\
      FROM [" + ais_db.name + "].[dbo].[aspnet_Users]\
      WHERE\
        UserName LIKE '" + free_access_config.login_mask + "'" + logins_query_condition + "\
    )\
    SELECT\
      UserId\
    FROM UsersList\
    WHERE seq = " + current_user_seq);

  if (!aisu.EOF) {
    var ais_user_query = ais_db.query("SELECT TOP 1\
      aisusers.UserName,\
      aism.Email,\
      aisup.LastName,\
      aisup.FirstName,\
      aisup.MiddleName\
    FROM [" + ais_db.name + "].[dbo].[aspnet_Users] aisusers\
    INNER JOIN [" + ais_db.name + "].[dbo].[aspnet_Membership] aism ON\
      aism.UserId = aisusers.UserId\
    INNER JOIN [AISArchive].[dbo].[tblUserProfiles] aisup ON\
      aisup.UserId = aisusers.UserId\
    WHERE\
      aisusers.UserId = '" + aisu.Fields.Item('UserId') + "'\
    ");
    access_log.add("�������� ���� ������� � ������������ ���������� � ������������ " +
      ais_user_query.Fields.Item('LastName') + " " +
      ais_user_query.Fields.Item('FirstName') + " " +
      ais_user_query.Fields.Item('MiddleName') + " (" +
      ais_user_query.Fields.Item('UserName') + "), ID " +
      aisu.Fields.Item('UserId'), "info");

    // ������� ����� �� ������ ����������
    var delete_role_query = ais_db.query("\
      DELETE\
        FROM [" + ais_db.name + "].[dbo].[aspnet_UsersInRoles]\
      WHERE\
        UserId = '" + aisu.Fields.Item('UserId') + "'\
        AND RoleId = '" + payments_config.role_id + "'\
    ");

  }
} while (!aisu.EOF)

access_log.add("���������� �������������: " + (current_user_seq - 1), "info");
ais_db.disconnect();
access_log.close();