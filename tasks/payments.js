var payments_config = AISAutomation.config.operations.payments;
var payments_log = new AISAutomation.log(payments_config.log);
var ais_db = new AISAutomation.db(AISAutomation.config.ais.db, payments_log);
ais_db.connect();

var payments = AISAutomation.fs.textFileToString(payments_config.payments_file);
// �������������� ������ � JSON ������
try {
  payments = JSON.parse(payments);
  var new_payments = [];
  for (var i = 0; i < payments.length; i++) {
    // ��������� ���������� � ������, ���� ����������
    // � ������� �������� ��������
    if (typeof(payments[i]) == 'object') {
      payments_log.add('�������� ������� ����������� ��������', 'debug');
      // ��������� ������� ����� ����� �������� ������� + ����� ���������
      // � ������� ��������. ������� ���������� ���� �� �������� ������� + 1.
      var current_time = new XDate();
      var access_open_time = new XDate(payments[i].access_open_time);
      var prolongated_access = access_open_time.addDays(payments[i].prolongate);
      var time_to_close = prolongated_access.diffDays(current_time);
      time_to_close -= payments[i].days + 1;
      // ���� ���������� �������� ������ 0, �� ���� ��������� ������
      if (time_to_close > 0) {
        payments_log.add('����� � �� ���������� � ������������ ' + payments[i].login, 'info');
        var ais_user_query = ais_db.query("SELECT TOP 1\
          aisusers.UserId,\
          aism.Email,\
          aisup.LastName,\
          aisup.FirstName,\
          aisup.MiddleName\
        FROM [" + ais_db.name + "].[dbo].[aspnet_Users] aisusers\
        INNER JOIN [" + ais_db.name + "].[dbo].[aspnet_Membership] aism ON\
          aism.UserId = aisusers.UserId\
        INNER JOIN [AISArchive].[dbo].[tblUserProfiles] aisup ON\
          aisup.UserId = aisusers.UserId\
        WHERE\
          aisusers.UserName LIKE '" + payments[i].login + "'\
        ");
        if (!ais_user_query.EOF) {
          // ��������� �������� � ������������
          var ais_user = {
            id:          AISAutomation.db.not_null(ais_user_query.Fields.Item('UserId')),
            email:       AISAutomation.db.not_null(ais_user_query.Fields.Item('Email')),
            last_name:   AISAutomation.db.not_null(ais_user_query.Fields.Item('LastName')),
            first_name:  AISAutomation.db.not_null(ais_user_query.Fields.Item('FirstName')),
            middle_name: AISAutomation.db.not_null(ais_user_query.Fields.Item('MiddleName'))
          };
          payments_log.add('�������� ����������� �������� ���������� �� ���������� ��� ������������ ' +
            payments[i].login + ' (' + ais_user.last_name + ' ' +
            ais_user.first_name + ' ' + ais_user.middle_name + ')', 'info');
          // �������� ����������� �������� ���������� �� ����������
          var close_requests = ais_db.query("DELETE\
            FROM [" + ais_db.name + "].[dbo].[aspnet_UsersInRoles]\
            WHERE\
              UserId = '" + ais_user.id + "'\
              AND RoleId = '" + payments_config.role_id + "'"
          );
          // ���� � ������ ������������ ������ e-mail, ��������� ��� ���������
          // � �������� ������� � ����������� ���
          if (ais_user.email !== '') {
            payments_log.add('�������� ��������� � �������� ������� � ����������� ��� ' +
              ' ������������ ' + payments[i].login + ' (' + ais_user.last_name +
              ' ' + ais_user.first_name + ' ' + ais_user.middle_name +
              ') �� ����� ' + ais_user.email, 'info');
            try {
              // ���������� ����� ���������
              var last_char = ais_user.first_name.toLowerCase().substr(ais_user.first_name.length - 1);
              var appeal = last_char == '�' || last_char == '�' || ais_user.first_name.toLowerCase() == '������' ? '���������' : '���������';
              var close_msg = appeal + ' ' + ais_user.first_name + ' ' +
                ais_user.middle_name + '!\n\n������ � ������� "���������� ���" � ��� ����� ������ � ����� � ���������� �������� ����������� �������.\n\n� ���������, �����-����������.';
              AISAutomation.email.send(AISAutomation.config.mail, ais_user.email, '��������� ������� � ������� ���������� ���', close_msg);
              ais_db.sleep(5000);           
            } catch(e) {
              payments_log.add('�������� ��������� �� �������. ' + e.message, 'warn');
            }
          }
        }
      } else { // ���� time_to_close <= 0, �� �������� �������� � ���������
               // ��� ���������� ������ � ������ ��������.
        new_payments.push(payments[i]);
      }
    }
  }
  payments_log.add("��������� ������ ����� payments.json", "info");
  payments_file = AISAutomation.fs.fso.OpenTextFile(".\\payments.json", 2, true, -2);
  payments_file.Write(JSON.stringify(new_payments, undefined, 2));
  payments_file.Close();
} catch(e) {
  var err_msg = '�������� ������ ������� ������ � ����� �� ���������� �� �������. ' + e.name + ': ' + e.message;
  payments_log.add(err_msg, 'warn');
  payments_log.add('�������� ��������� � �������� �������������� �� e-mail ' + AISAutomation.config.mail.admin_email, 'info');
  AISAutomation.email.send(AISAutomation.config.mail, AISAutomation.config.mail.admin_email, '������ �������� ������� � ����������� ���', err_msg);
  ais_db.sleep(5000);
}
ais_db.disconnect();
payments_log.close();